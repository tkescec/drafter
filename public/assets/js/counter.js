function counter(field, time, button) {

    // access to html element
    var counter_field = document.getElementById(field);
    var button_field = document.getElementById(button);

    // destination date/time
    var date_stop = time; //new Date(2013,6,7,0,0,0,0);

    // call counting function every second (1000 ms)
    var counting_loop = setInterval(counting, 1000);

    // counting function
    function counting()
    {
        // current date/time
        var date_now = new Date();

        if( date_stop == null ) {
            var text = "- you forget date/time -";
        } else {

            // convert miliseconds to seconds
            var diff = Math.round((date_stop-date_now)/1000);

            // still in future
            if( (0 < diff) )
            {
                // convert diff to days, hours, minutes, seconds

                // seconds
                var seconds = diff % 60;
                diff = (diff-seconds) / 60;
                if( seconds < 10 ) seconds = "0" + seconds;

                // minutes
                var minutes = diff % 60;
                diff = (diff-minutes) / 60;
                if( minutes < 10 ) minutes = "0" + minutes;

                // hours
                var hours = diff % 24;
                diff = (diff-hours) / 24;
                if( hours < 10 ) hours = "0" + hours;

                // days
                var days = diff;
                if( days < 10 ) days = "0" + days;

                // convert to text
                text = days+" days " +hours+":"+minutes+":"+seconds;
            } else {
                // removing counter
                button_field.classList.add("disabled");
                button_field.removeAttribute("href");
                button_field.removeAttribute("id");
                counter_field.innerHTML = "THE END";

                unsetInterval(counting_loop);

            }
        }
        // put text into html
        counter_field.innerHTML = text;
    }
}
