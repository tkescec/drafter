<?php namespace App\Services\Validators;

class LoginValidator extends Validator {

    public static $rules = array(
        'email' => 'required|email',
        'password'  => 'required|max:255',
    );
}