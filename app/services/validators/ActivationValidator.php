<?php namespace App\Services\Validators;

class ActivationValidator extends Validator {

    public static $rules = array(
        'name' => 'required|unique:users,nickname|min:3|max:32',
        'password' => 'required|min:8',
        'confirm_password' => 'required|min:8|same:password'
    );
}