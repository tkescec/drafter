<?php namespace App\Services\Validators;

class LeaguesValidator extends Validator {

    public static $rules = array(
        'league-title' => 'required|max:255',
        'salary-cap'  => 'required',
        'season'  => 'required',
        'round'  => 'required',
        'entry-fee'  => 'required',
        'max-players'  => 'required|numeric',
        'application-date'  => 'required',
        'application-time'  => 'required',
    );
}