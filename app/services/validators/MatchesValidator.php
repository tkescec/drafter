<?php namespace App\Services\Validators;

class MatchesValidator extends Validator {

    public static $rules = array(
        'home' => 'required',
        'away'  => 'required',
        'match-date'  => 'required',
        'match-time'  => 'required',
    );
}