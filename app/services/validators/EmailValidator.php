<?php namespace App\Services\Validators;

class EmailValidator extends Validator {

    public static $rules = array(
        'email' => 'required|email|unique:users,email'
    );

}