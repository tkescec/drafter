<?php namespace App\Services\Validators;

class NewsValidator extends Validator {

    public static $rules = array(
        'news-title' => 'required|max:255',
        'news-content'  => 'required',
    );
}