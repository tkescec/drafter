<?php namespace App\Services\Validators;

class ForgotValidator extends Validator {

    public static $rules = array(
        'email' => 'required|email'
    );

}