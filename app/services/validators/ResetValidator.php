<?php namespace App\Services\Validators;

class ResetValidator extends Validator {

    public static $rules = array(
        'password' => 'required|min:8',
        'confirm_password' => 'required|min:8|same:password'
    );
}