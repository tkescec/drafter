<?php namespace App\Services\Validators;

class SubscriberValidator extends Validator {

    public static $rules = array(
        'email' => 'required|email'
    );

}