<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as' => '/', 'uses' => 'App\Controllers\HomeController@index'));
Route::get('rules', array('as' => 'rules', 'uses' => 'App\Controllers\HomeController@rules'));

Route::post('newsletter/subscribe', array('as' => 'newsletter.subscribe', 'uses' => 'App\Controllers\Newsletter\NewsletterController@subscribe'));
Route::get('newsletter/unsubscribe/{email}', array('as' => 'newsletter.unsubscribe', 'uses' => 'App\Controllers\Newsletter\NewsletterController@unsubscribePage'));
Route::post('newsletter/unsubscribe', array('as' => 'newsletter.unsubscribe.post', 'uses' => 'App\Controllers\Newsletter\NewsletterController@unsubscribe'));

Route::get('auth/login', array('as' => 'auth.login', 'uses' => 'App\Controllers\Auth\AuthController@getLogin'));
Route::post('auth/login', array('before' => 'csrf','as' => 'auth.login.post', 'uses' => 'App\Controllers\Auth\AuthController@postLogin'));
Route::get('auth/logout', array('as' => 'auth.logout', 'uses' => 'App\Controllers\Auth\AuthController@getLogout'));

Route::get('auth/forgot_password', array('as' => 'auth.forgot_password', 'uses' => 'App\Controllers\Auth\AuthController@getForgot'));
Route::post('auth/forgot_password', array('before' => 'csrf','as' => 'auth.forgot_password.post', 'uses' => 'App\Controllers\Auth\AuthController@postForgot'));

Route::get('auth/reset_password/{resetcode}', array('as' => 'auth.reset_password', 'uses' => 'App\Controllers\Auth\AuthController@getReset'));
Route::post('auth/reset_password/{resetcode}', array('before' => 'csrf','as' => 'auth.reset_password.post', 'uses' => 'App\Controllers\Auth\AuthController@postReset'));

Route::get('auth/register', array('as' => 'auth.register', 'uses' => 'App\Controllers\Auth\RegController@getRegistration'));
Route::post('auth/register',  array('before' => 'csrf','as' => 'auth.register.post', 'uses' => 'App\Controllers\Auth\RegController@postRegistration'));

Route::get('auth/activation/{activationcode}', array('as' => 'auth.activation', 'uses' => 'App\Controllers\Auth\RegController@getActivation'));
Route::post('auth/activation/{activationcode}', array('before' => 'csrf','as' => 'auth.activation.post', 'uses' => 'App\Controllers\Auth\RegController@postActivation'));

Route::group(array('before' => 'auth.user'), function(){

    Route::get('user/dashboard', array('as' => 'user.dashboard', 'uses' => 'App\Controllers\User\UserController@dashboard'));
    Route::get('user/game/edit/{id}', array('as' => 'user.game', 'uses' => 'App\Controllers\User\UserController@gameEdit'));
    Route::get('user/game/results/{id}', array('as' => 'user.game.results', 'uses' => 'App\Controllers\User\UserController@gameResults'));
    Route::get('user/game/team/{id}', array('as' => 'user.game.team', 'uses' => 'App\Controllers\User\UserController@gameTeam'));
    Route::put('user/game/update', array('as' => 'user.game.update', 'uses' => 'App\Controllers\User\UserController@gameUpdate'));
    Route::post('user/game/position', array('as' => 'user.game.position', 'uses' => 'App\Controllers\User\UserController@gamePosition'));
    Route::post('user/game/stats', array('as' => 'user.game.stats', 'uses' => 'App\Controllers\User\UserController@gameStats'));

});

Route::get('leagues', array('as' => 'leagues', 'uses' => 'App\Controllers\Leagues\LeaguesController@index'));
Route::post('leagues/matches', array('as' => 'leagues/matches', 'uses' => 'App\Controllers\Leagues\LeaguesController@matches'));
Route::post('leagues/prizes', array('as' => 'leagues/prizes', 'uses' => 'App\Controllers\Leagues\LeaguesController@prizes'));
Route::post('leagues/players', array('as' => 'leagues/players', 'uses' => 'App\Controllers\Leagues\LeaguesController@players'));
Route::get('draft/{id}/{type}', array('as' => 'draft', 'uses' => 'App\Controllers\Draft\DraftController@index'));
Route::post('draft/filter', array('as' => 'draft.filter', 'uses' => 'App\Controllers\Draft\DraftController@playerFilter'));
Route::post('draft/search', array('as' => 'draft.search', 'uses' => 'App\Controllers\Draft\DraftController@playerSearch'));
Route::post('draft/resetfilter', array('as' => 'draft.resetfilter', 'uses' => 'App\Controllers\Draft\DraftController@resetFilter'));
Route::post('draft/check', array('as' => 'draft.check', 'uses' => 'App\Controllers\Draft\DraftController@checkLogin'));
Route::post('draft/invite', array('as' => 'draft.invite', 'uses' => 'App\Controllers\Draft\DraftController@sendInvitation'));


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/

Route::get('admin', array('as' => 'admin', 'uses' => 'App\Controllers\Admin\AuthController@index'));
Route::post('admin', array('as' => 'admin.post', 'uses' => 'App\Controllers\Admin\AuthController@postLogin'));
Route::get('admin/logout', array('as' => 'admin.logout', 'uses' => 'App\Controllers\Admin\AuthController@getLogout'));

Route::group(array('before' => 'auth.admin'), function(){

    Route::get('admin/dashboard', array('as' => 'admin.dashboard', 'uses' => 'App\Controllers\Admin\DashboardController@index'));
    Route::get('admin/dashboard/points/{id}', array('as' => 'admin.dashboard.points', 'uses' => 'App\Controllers\Admin\PointsController@index'));
    Route::post('admin/dashboard/points/{id}', array('as' => 'admin.dashboard.points.store', 'uses' => 'App\Controllers\Admin\PointsController@store'));
    Route::get('admin/dashboard/points/{league_id}/{id}', array('as' => 'admin.dashboard.points.edit', 'uses' => 'App\Controllers\Admin\PointsController@edit'));
    Route::get('admin/dashboard/results/{id}', array('as' => 'admin.dashboard.results', 'uses' => 'App\Controllers\Admin\MatchesController@index'));
    Route::put('admin/dashboard/results/{id}', array('as' => 'admin.dashboard.results.update', 'uses' => 'App\Controllers\Admin\MatchesController@update'));
    Route::get('admin/leagues', array('as' => 'admin.leagues', 'uses' => 'App\Controllers\Admin\LeaguesController@index'));
    Route::get('admin/leagues/create', array('as' => 'admin.leagues.create', 'uses' => 'App\Controllers\Admin\LeaguesController@create'));
    Route::get('admin/leagues/edit/{id}', array('as' => 'admin.leagues.edit', 'uses' => 'App\Controllers\Admin\LeaguesController@edit'));
    Route::post('admin/leagues', array('as' => 'admin.leagues.store', 'uses' => 'App\Controllers\Admin\LeaguesController@store'));
    Route::put('admin/leagues/{id}', array('as' => 'admin.leagues.update', 'uses' => 'App\Controllers\Admin\LeaguesController@update'));
    Route::delete('admin/leagues/{id}', array('as' => 'admin.leagues.destroy', 'uses' => 'App\Controllers\Admin\LeaguesController@destroy'));
    Route::get('admin/news/create', array('as' => 'admin.news.create', 'uses' => 'App\Controllers\Admin\NewsController@create'));
    Route::post('admin/news', array('as' => 'admin.news.store', 'uses' => 'App\Controllers\Admin\NewsController@store'));
    Route::get('admin/news/edit/{id}', array('as' => 'admin.news.edit', 'uses' => 'App\Controllers\Admin\NewsController@edit'));
    Route::put('admin/news/{id}', array('as' => 'admin.news.update', 'uses' => 'App\Controllers\Admin\NewsController@update'));
    Route::delete('admin/news/{id}', array('as' => 'admin.news.destroy', 'uses' => 'App\Controllers\Admin\NewsController@destroy'));
    Route::get('admin/matches/create/{id}', array('as' => 'admin.matches.create', 'uses' => 'App\Controllers\Admin\MatchesController@create'));
    Route::post('admin/matches', array('as' => 'admin.matches.store', 'uses' => 'App\Controllers\Admin\MatchesController@store'));
    Route::get('admin/matches/edit/{id}', array('as' => 'admin.matches.edit', 'uses' => 'App\Controllers\Admin\MatchesController@edit'));
    Route::put('admin/matches/{id}', array('as' => 'admin.matches.update', 'uses' => 'App\Controllers\Admin\MatchesController@updateMatch'));
    Route::delete('admin/matches/create/{id}', array('as' => 'admin.matches.destroy', 'uses' => 'App\Controllers\Admin\MatchesController@destroy'));

});