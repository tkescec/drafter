<?php namespace App\Models;

class PlayerPoints extends \Eloquent {

    protected $table = 'player_points';
    public $timestamps = false;

    public function players()
    {
        return $this->belongsTo('App\Models\Players','player_id');
    }

}