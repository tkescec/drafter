<?php namespace App\Models;

class Games extends \Eloquent {

    protected $table = 'games';
    public $timestamps = false;


    public function leagues()
    {
        return $this->belongsTo('App\Models\Leagues','league_id');
    }

    public function competitors()
    {
        return $this->belongsTo('App\Models\User','user_id');
    }
}