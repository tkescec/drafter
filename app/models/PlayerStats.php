<?php namespace App\Models;

class PlayerStats extends \Eloquent {

    protected $table = 'player_stats';
    public $timestamps = false;

    public function players()
    {
        return $this->belongsTo('App\Models\Players','player_id');
    }

}