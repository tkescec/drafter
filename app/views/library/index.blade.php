<div class="modal fade" id="library" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">library</h2>
            </div>
            <div class="modal-body">
                <h1 style="font-size:1.6rem;">Upload new image</h1>
                {{ Form::file('league-logo-file') }}
                <hr>
                <h1 style="font-size:1.6rem;">Select an image from library</h1>
                <ul class="library_image_list">
                @foreach($files as $file)
                    <li>
                        <img src="{{'http://'.$_SERVER['HTTP_HOST'].'/'.$file}}" class="library_img">
                    </li>
                @endforeach
                </ul>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px;">Cancel</button>
                <button type="button" class="btn btn-success" data-dismiss="modal" >Add</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $(".library_img").click(function(){
            if($(this).hasClass('selected')){
                $(this).removeClass('selected');
            }
            else{
                $('.library_img').removeClass('selected');
                $(this).addClass('selected');
            }
        });
        $(".btn-success").click(function(){
            var path = $(".selected").attr('src');
            $("#logo").attr('src',path);
            $("#league-logo").val(path);
            $("#add_logo").html('change logo');
        });
    });
</script>