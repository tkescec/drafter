@extends ('_layouts.rules')


@section('main')


<section class="game-rules">

    <h1>Game rules</h1>
    <ol>
        <li>Register or login</li>
        <li>Select a content. Choose a league you want to play with different entry fee, prizes or number of participans</li>
        <li>Build a team. Pick 11 players optional, you just need to stay under the salary cap</li>
        <li>Watch the games. Watch matches and track the performance of your players</li>
        <li>Win prizes. When all games end, your players will earn fantasy points based on their performances in real matches. If you finish on top, you win the prizes!</li>
    </ol>

    <h1>League rules</h1>
    <ul>
        <li>Each contest requires selecting 11 players, there is no substitutions</li>
        <li>If the player you have selected is not performed, you earn 0 fantasy points for that player</li>
        <li>Formations you can select : 3-4-3, 4-4-2, 4-3-3</li>
        <li>The positions to choose players are : Goalkeeper (GK), Defender (DF), Midfielder (MF) and Attacker (AT)</li>
        <li>You can choose every player you want from Premier League</li>
        <li>Player salary is determined by the actual value per transfermarkt</li>
        <li>Rosters of your opponents are invisible, until the first game kick off</li>
        <li>Each contest specifies a date after which entries may not be added, edited, or cancelled</li>
        <li>When all games in the contest end, players will earn fantasy points based on their performances in real matches. Participans who scored the most points win the prizes.</li>
        <li>If a match that is part of a contest is rescheduled outside of the originally scheduled beginning and end of the contest, players in those matches will earn zero points</li>
    </ul>

    <h1>Scoring table</h1>
    <table>
        <thead>
            <tr>
                <td colspan="2">All positions</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Minutes</td>
                <td>0.04</td>
            </tr>
            <tr>
                <td>Yellow card</td>
                <td>-1.5</td>
            </tr>
            <tr>
                <td>Red card</td>
                <td>-5</td>
            </tr>
            <tr>
                <td>Own goal</td>
                <td>-5</td>
            </tr>
            <tr>
                <td>Asist</td>
                <td>4</td>
            </tr>
            <tr>
                <td>Accurate cross</td>
                <td>0.75</td>
            </tr>
            <tr>
                <td>Missed penalty</td>
                <td>-4</td>
            </tr>
            <tr>
                <td>Winning team</td>
                <td>1.5</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <td colspan="2">Attackers (AT)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Goal</td>
                <td>6</td>
            </tr>
            <tr>
                <td>Pass</td>
                <td>0.05</td>
            </tr>
            <tr>
                <td>Shot on goal (on target)</td>
                <td>1.5</td>
            </tr>
            <tr>
                <td>Touches</td>
                <td>0.02</td>
            </tr>
            <tr>
                <td>Interception</td>
                <td>0.25</td>
            </tr>
            <tr>
                <td>Cleareance</td>
                <td>0.25</td>
            </tr>
            <tr>
                <td>Dribling</td>
                <td>0.25</td>
            </tr>
            <tr>
                <td>Offside</td>
                <td>-0.25</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <td colspan="2">Midfielders (MF)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Goal</td>
                <td>6</td>
            </tr>
            <tr>
                <td>Pass</td>
                <td>0.05</td>
            </tr>
            <tr>
                <td>Shot on goal (on target)</td>
                <td>1.25</td>
            </tr>
            <tr>
                <td>Touch</td>
                <td>0.02</td>
            </tr>
            <tr>
                <td>Interception</td>
                <td>0.75</td>
            </tr>
            <tr>
                <td>Goal received</td>
                <td>-0.5</td>
            </tr>
            <tr>
                <td>Cleareance</td>
                <td>0.25</td>
            </tr>
            <tr>
                <td>Dribling</td>
                <td>0.25</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <td colspan="2">Defenders (DF)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Goal</td>
                <td>6</td>
            </tr>
            <tr>
                <td>Pass</td>
                <td>0.04</td>
            </tr>
            <tr>
                <td>Shot on goal (on target)</td>
                <td>1</td>
            </tr>
            <tr>
                <td>Block</td>
                <td>1.5</td>
            </tr>
            <tr>
                <td>Interception</td>
                <td>1</td>
            </tr>
            <tr>
                <td>Cleareance</td>
                <td>0.25</td>
            </tr>
            <tr>
                <td>Clean sheet</td>
                <td>3</td>
            </tr>
            <tr>
                <td>Goal received</td>
                <td>-1</td>
            </tr>
        </tbody>
    </table>
    <table>
        <thead>
            <tr>
                <td colspan="2">Goalkeepers (GK)</td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>Goal scored</td>
                <td>6</td>
            </tr>
            <tr>
                <td>1st goal received</td>
                <td>-1</td>
            </tr>
            <tr>
                <td>Every next goal received</td>
                <td>-3</td>
            </tr>
            <tr>
                <td>Save</td>
                <td>1.5</td>
            </tr>
            <tr>
                <td>Claim</td>
                <td>1.25</td>
            </tr>
            <tr>
                <td>Penalty saved</td>
                <td>5</td>
            </tr>
            <tr>
                <td>Clean sheet</td>
                <td>3</td>
            </tr>
        </tbody>
    </table>

</section>


@stop
