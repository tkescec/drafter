@extends ('_layouts.forgot_password')

@section('main')

{{ Form::open(array('id'=>'forgot-form', 'class'=>'standalone-form')) }}
@if($errors->any())
<div class="alert alert-danger" role="alert">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif
<fieldset>
    <div class="standalone-form__form-field">
        {{ Form::email('email', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter email', 'id'=>'vermail')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::submit('Confirm email', array('class' => 'form-field__input_submit cta-button disabled','disabled', 'id'=>'vermail-btn')) }}
    </div>
</fieldset>
{{ Form::close() }}

<script>
    $( "#vermail" ).keyup(function() {
        var email = $("#vermail").val();
        if(isValidEmailAddress(email))
        {
            $("#vermail-btn").removeAttr("disabled");
            $("#vermail-btn").removeClass("disabled");
            $("#vermail-btn").addClass("green");
        }
        else
        {
            $( "#vermail-btn" ).attr("disabled","disabled");
            $("#vermail-btn").removeClass("green");
            $("#vermail-btn").addClass("disabled");
        }
    });
    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }
</script>

@stop