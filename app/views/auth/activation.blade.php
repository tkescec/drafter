@extends ('_layouts.activation')

@section('main')

{{ Form::open(array('id'=>'activation-form', 'class'=>'standalone-form')) }}
@if($errors->any())
<div class="alert alert-danger" role="alert">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif

<fieldset>
    {{Form::hidden('leagueId',Input::get('league_id'))}}
    {{Form::hidden('leagueType',Input::get('league_type'))}}
    <div class="standalone-form__form-field">
        {{ Form::text('name', '', array('class' => 'form-field__input_text', 'placeholder' => 'Name', 'id'=>'nickname')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::password('password', array('class' => 'form-field__input_text', 'placeholder' => 'Enter password')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::password('confirm_password', array('class' => 'form-field__input_text', 'placeholder' => 'Confirm password')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::submit('Create account', array('class' => 'form-field__input_submit cta-button green')) }}
    </div>
</fieldset>

{{ Form::close() }}

@stop