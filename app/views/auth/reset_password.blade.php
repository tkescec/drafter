@extends ('_layouts.reset_password')

@section('main')

{{ Form::open(array('id'=>'reset-form', 'class'=>'standalone-form')) }}
@if($errors->any())
<div class="alert alert-danger" role="alert">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif

<fieldset>

    <div class="standalone-form__form-field">
        {{ Form::password('password', array('class' => 'form-field__input_text', 'placeholder' => 'Enter password')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::password('confirm_password', array('class' => 'form-field__input_text', 'placeholder' => 'Confirm password')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::submit('Set password', array('class' => 'form-field__input_submit cta-button green')) }}
    </div>
</fieldset>

{{ Form::close() }}

@stop