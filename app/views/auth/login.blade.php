@extends ('_layouts.login')

@section('main')
<?php
    $data = Input::all();
    if(empty($data)){
        $data['league_id'] = null;
    }
?>
{{ Form::open(array('id'=>'login-form', 'class'=>'standalone-form')) }}
    @include('bones-flash::bones.flash')
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </div>
    @endif
    <fieldset>
        @if($data)
            {{Form::hidden('leagueId',$data['league_id'])}}
        @endif
        <div class="standalone-form__form-field">
            {{ Form::email('email', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter email')) }}
        </div>
        <div class="standalone-form__form-field">
            {{ Form::password('password', array('class' => 'form-field__input_text', 'placeholder' => 'Enter password')) }}
        </div>
        <div class="standalone-form__form-field">
            <label class="form-field__label">
                {{ Form::checkbox('remember', '', array('class' => 'form-field__input_checkbox')) }}
                Remember me for 2 weeks
            </label>
        </div>
        <div class="standalone-form__form-field">
            {{ Form::submit('Login', array('class' => 'form-field__input_submit cta-button green')) }}
            <!-- <a href="dashboard.php" class="form-field__input_submit cta-button green">Login</a>

             <input type="submit" class="form-field__input_submit cta-button" placeholder="Login" value="login">
             -->
        </div>
        <div class="standalone-form__form-field">
            <a href="{{ URL::route('auth.forgot_password') }}" class="form-field__password-reset">Forgot password</a>
        </div>
        <hr style="margin:80px 0;">
        <div class="standalone-form__form-field">
            <a href="{{ URL::route('auth.register',array('league_id' => $data['league_id'])) }}" class="form-field__input_submit cta-button black">create new account</a>
            <!--
            <input type="submit" class="form-field__input_submit cta-button" placeholder="Login" value="login">
            -->
        </div>
    </fieldset>
{{ Form::close() }}
@stop