@extends ('admin._layouts.leagues')

@section('main')
<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-8">
            <h1 style="margin:0!important;">Leagues</h1>
        </div>
        <div class="col-md-4">
            <a href="{{ URL::route('admin.leagues.create') }}" class="form-field__input_submit cta-button black">create new league</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            @include('bones-flash::bones.flash')
            <div class="table-responsive" style="border:none;">
                @if ($leagues->count() == 0)
                <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no active leagues.</p>
                @endif
                @if ($leagues->count() > 0)
                <table class="table">
                    <tr style="font-weight:bold;">
                        <th>Logo</th>
                        <th>League</th>
                        <th>Season</th>
                        <th>Round</th>
                        <th>Salary Cap</th>
                        <th>Entry Fee</th>
                        <th>Number of Players</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($leagues as $league)
                    <?php
                    $d = explode('-',$league->date);
                    $f = $d[2].'/'.$d[1].'/'.$d[0];
                    ?>
                    <tr>
                        <td><img src="{{ $league->logo }}" alt="" style="width:100px;"></td>
                        <td>{{ $league->title }}</td>
                        <td>{{ $league->season }}</td>
                        <td>{{ $league->round }}</td>
                        <td>€{{ $league->salary_cup }}M</td>
                        <td>{{ $league->entry_fee }}</td>
                        <td>{{ $league->active_players }}/{{ $league->num_players }}</td>
                        <td>{{ $f }}</td>
                        <td>{{ $league->time }}</td>
                        <td>
                            <a href="{{ URL::route('admin.leagues.edit', $league->id) }}">
                                <i class="fa fa-pencil"></i>
                            </a>&nbsp;
                            <a href="#" data-toggle="modal" data-target="#delete{{$league->id;}}">
                                <i class="fa fa-trash-o"></i>
                            </a>
                            <!------------------ Delete league dialog---------------->
                            @if ($league->count() > 0)
                            <div class="modal fade" id="delete{{$league->id;}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="text-align:center;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">warning!</h2>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to delete a <b>&quot;{{ $league->title }}&quot;</b> league?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px;">No</button>
                                            {{ Form::open(array('method' => 'DELETE', 'action' => array('admin.leagues.destroy', $league->id), 'class' => 'pull-right')) }}
                                            {{ Form::submit('Yes , I&rsquo;m sure', array('class' => 'btn btn-danger')) }}
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!------------------ End Delete tournament dialog---------------->
                        </td>
                    </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
