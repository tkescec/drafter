@extends ('admin._layouts.leagues')

@section('main')
<div class="container-fluid">
<hr>
<h1>Edit {{$league->title}}</h1>
<hr>
<?php
    $date = $league->date;
    $explodedate = explode('-', $date);
    $newdate = $explodedate[2].'/'.$explodedate[1].'/'.$explodedate[0];
?>
{{ Form::open(array('method' => 'PUT', 'action' => array('admin.leagues.update', $league->id),'id'=>'leagues-form', 'class'=>'standalone-form', 'style' => 'margin:0 auto')) }}
@include('bones-flash::bones.flash')
@if($errors->any())
<div class="alert alert-danger" role="alert">
    <a href="#" class="close" data-dismiss="alert">&times;</a>
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif
@include('library.index')
<fieldset>
    <div class="standalone-form__form-field" style="text-align:center;">
        <img src="{{ $league->logo }}" id="logo" style="margin:10px auto;border:solid 5px #ddd; max-width:200px;">
        <a href="#" data-toggle="modal" data-target="#library" class="form-field__input_submit cta-button black" id="add_logo">
            change logo
        </a>
        {{ Form::hidden('league-logo', $league->logo, array('id' => 'league-logo')) }}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('league-title', 'League Title')}}
        {{ Form::text('league-title', $league->title, array('class' => 'form-field__input_text', 'placeholder' => 'League Title')) }}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('season', 'Season')}}
        {{Form::select('season', array('14/15' => '2014/2015', '15/16' => '2015/2016', '16/17' => '2016/2017', '17/18' => '2017/2018'), $league->season, array('class' => 'form-field__input_text'))}}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('round', 'Round')}}
        {{Form::select('round', array('Semi-finals' => 'Semi-finals','Finals' => 'Finals','1' => '1', '2' => '2', '3' => '3', '4' => '4', '5' => '5', '6' => '6', '7' => '7', '8' => '8', '9' => '9', '10' => '10', '11' => '11', '12' => '12', '13' => '13', '14' => '14', '15' => '15', '16' => '16', '17' => '17', '18' => '18', '19' => '19', '20' => '20', '21' => '21', '22' => '22', '23' => '23', '24' => '24', '25' => '25', '26' => '26', '27' => '27', '28' => '28', '29' => '29', '30' => '30', '31' => '31', '32' => '32', '33' => '33'), $league->round, array('class' => 'form-field__input_text'))}}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('type', 'Type')}}
        {{Form::select('type', array('1' => 'Champions League','2' => 'Premier League'), $league->type, array('class' => 'form-field__input_text'))}}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('salary-cap', 'Salary Cap')}}
        {{Form::select('salary-cap', array('50' => '€50M', '100' => '€100M', '150' => '€150M', '200' => '€200M'), $league->salary_cup, array('class' => 'form-field__input_text'))}}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('entry-fee', 'Entry Fee')}}
        {{Form::select('entry-fee',  array('Free' => 'Free', '€5' => '€5', '€10' => '€10', '€50' => '€50'), $league->entry_fee, array('class' => 'form-field__input_text'))}}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('max-players', 'Max Players')}}
        {{ Form::text('max-players', $league->num_players, array('class' => 'form-field__input_text', 'placeholder' => 'Max Players')) }}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('application-date', 'Application Date')}}
        {{ Form::text('application-date', $newdate, array('class' => 'form-field__input_text', 'placeholder' => 'Application Date', 'id'=>'application-date')) }}
    </div>
    <div class="standalone-form__form-field">
        {{Form::label('application-time', 'Application Time')}}
        {{ Form::text('application-time', $league->time, array('class' => 'form-field__input_text', 'placeholder' => 'Application Time', 'id'=>'application-time')) }}
    </div>
    <div class="standalone-form__form-field clearfix">
        {{ Form::submit('Save', array('class' => 'form-field__input_submit cta-button green left')) }}
        <a href="{{URL::route('admin.dashboard')}}" class="form-field__input_submit cta-button btn-danger right">Cancle</a>
    </div>
</fieldset>
{{ Form::close() }}
    </div>
<script>
    $(document).ready(function() {
        $('#application-date').datepicker({
            format: "dd/mm/yyyy"
        });
        $('#application-time').timepicker({
            'timeFormat': 'H:i' ,
            'step': 15
        });
    });
</script>
@stop
