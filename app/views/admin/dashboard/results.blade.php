@extends ('admin._layouts.results')

@section('main')
<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-10">
            <h1>Match results</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ URL::route('admin.dashboard') }}" class="form-field__input_submit cta-button black">Back to Dashboard</a>
        </div>
    </div>
    <hr>
    @include('bones-flash::bones.flash')
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </div>
    @endif
    <div class="table-responsive">
    <table class="table">
        <tr style="font-weight:bold;">
            <th>Home</th>
            <th>Result</th>
            <th>Away</th>
            <th>Date</th>
            <th>Time</th>
            <th>Action</th>
        </tr>
    @foreach ($matches as $match)
        <tr>
    {{ Form::open(array('method' => 'PUT', 'action' => array('admin.dashboard.results.update', $match->id), 'class'=>'standalone-form', 'style' => 'margin:0 auto')) }}
            <td>{{$match->club1}}</td>
            <td>{{ Form::text('result', $match->results, array('class' => 'form-field__input_text', 'placeholder' => 'Match result', 'style' => 'width:80px;')) }}</td>
            <td>{{$match->club2}}</td>
            <td>{{$match->date}}</td>
            <td>{{$match->time}}</td>
            <td>
                <div class="standalone-form__form-field clearfix">
                    {{ Form::submit('Save', array('class' => 'form-field__input_submit cta-button green left')) }}
                </div>
            </td>
    {{ Form::close() }}
        </tr>
    @endforeach
    </table>
    </div>
</div>
@stop
