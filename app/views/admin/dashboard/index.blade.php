@extends ('admin._layouts.dashboard')

@section('main')
<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-10">
            <h1 style="margin:0!important;">Dashboard</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ URL::route('admin.logout') }}" class="form-field__input_submit cta-button black">Log Out</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                @include('bones-flash::bones.flash')
                <div style="padding-bottom:3rem;">
                    <div class="col-md-10">
                        <h2 style="font-size:2rem;">Leagues</h2>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ URL::route('admin.leagues.create') }}" class="form-field__input_submit cta-button green" style="font-size:1rem;padding:10px 0;">create new league</a>
                    </div>
                </div>
                @if ($leagues->count() == 0)
                <hr>
                <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no active leagues.</p>
                @endif
                @if ($leagues->count() > 0)
                <table class="table">
                    <tr style="font-weight:bold;">
                        <th>Logo</th>
                        <th>League</th>
                        <th>Season</th>
                        <th>Round</th>
                        <th>Salary Cap</th>
                        <th>Entry Fee</th>
                        <th>Number of Players</th>
                        <th>Date</th>
                        <th>Time</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($leagues as $league)
                    <?php
                    $d = explode('-',$league->date);
                    $f = $d[2].'/'.$d[1].'/'.$d[0];
                    $time_array = explode(':',$league->time);
                    $time = $time_array[0].':'.$time_array[1];
                    ?>
                    <tr>
                        <td><img src="{{ $league->logo }}" alt="" style="width:80px;"></td>
                        <td>{{ $league->title }}</td>
                        <td>{{ $league->season }}</td>
                        <td>{{ $league->round }}</td>
                        <td>€{{ $league->salary_cup }}M</td>
                        <td>{{ $league->entry_fee }}</td>
                        <td>{{ $league->active_players }}/{{ $league->num_players }}</td>
                        <td>{{ $f }}</td>
                        <td>{{ $time }}</td>
                        <td>
                            <a href="{{ URL::route('admin.leagues.edit', $league->id) }}" data-toggle="tooltip" title="Edit league">
                                <i class="fa fa-pencil"></i>
                            </a>&nbsp;
                            <a href="#" data-toggle="modal" data-target="#delete{{$league->id;}}">
                                <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete league"></i>
                            </a>&nbsp;
                            <a href="{{ URL::route('admin.dashboard.points', $league->id) }}" data-toggle="tooltip" title="Upload league data">
                                <i class="fa fa-upload"></i>
                            </a>&nbsp;
                            <a href="{{ URL::route('admin.dashboard.results', $league->id) }}" data-toggle="tooltip" title="Match results">
                                <i class="fa fa-futbol-o"></i>
                            </a>&nbsp;
                            <a href="{{ URL::route('admin.matches.create', $league->id) }}" data-toggle="tooltip" title="Add matches">
                                <i class="fa fa-bars"></i>
                            </a>
                            <!------------------ Delete league dialog---------------->
                            @if ($league->count() > 0)
                            <div class="modal fade" id="delete{{$league->id;}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content" style="text-align:center;">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                            <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">warning!</h2>
                                        </div>
                                        <div class="modal-body">
                                            <h5>Are you sure you want to delete a <b>&quot;{{ $league->title }}&quot;</b> league?</h5>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px;">No</button>
                                            {{ Form::open(array('method' => 'DELETE', 'action' => array('admin.leagues.destroy', $league->id), 'class' => 'pull-right')) }}
                                            {{ Form::submit('Yes , I&rsquo;m sure', array('class' => 'btn btn-danger')) }}
                                            {{ Form::close() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <!------------------ End Delete tournament dialog---------------->
                            <script>
                                $(function () {
                                    $('[data-toggle="tooltip"]').tooltip();

                                })
                            </script>
                        </td>
                    </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
    <hr><hr>
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <div style="padding-bottom:3rem;">
                    <div class="col-md-10">
                        <h2 style="font-size:2rem;">News Feed</h2>
                    </div>
                    <div class="col-md-2">
                        <a href="{{ URL::route('admin.news.create') }}" class="form-field__input_submit cta-button green" style="font-size:1rem;padding:10px 0;">add news</a>
                    </div>
                </div>
                @if ($news->count() == 0)
                <hr>
                <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no news feeds.</p>
                @endif
                @if ($news->count() > 0)
                <table class="table">
                    @foreach ($news as $value)
                    <?php
                    $timestamp = strtotime($value->created_at);
                    $date = date('d/m/Y', $timestamp);
                    $time = date('H:m', $timestamp);
                    ?>
                        <tr>
                            <td>{{ $value->title }}</td>
                            <td>{{ substr($value->content,0,200) }}</td>
                            <td>{{ $date }} {{ $time }}</td>
                            <td style="width:150px;">
                                <a href="{{ URL::route('admin.news.edit', $value->id) }}" data-toggle="tooltip" title="Edit news">
                                    <i class="fa fa-pencil"></i>
                                </a>&nbsp;
                                <a href="#" data-toggle="modal" data-target="#delete_news{{$value->id;}}">
                                    <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete news"></i>
                                </a>&nbsp;
                                <!------------------ Delete news dialog---------------->
                                @if ($value->count() > 0)
                                <div class="modal fade" id="delete_news{{$value->id;}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content" style="text-align:center;">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                                <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">warning!</h2>
                                            </div>
                                            <div class="modal-body">
                                                <h5>Are you sure you want to delete a <b>&quot;{{ $value->title }}&quot;</b> news?</h5>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px;">No</button>
                                                {{ Form::open(array('method' => 'DELETE', 'action' => array('admin.news.destroy', $value->id), 'class' => 'pull-right')) }}
                                                {{ Form::submit('Yes , I&rsquo;m sure', array('class' => 'btn btn-danger')) }}
                                                {{ Form::close() }}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endif
                                <!------------------ End Delete tournament dialog---------------->
                            </td>
                        </tr>
                    @endforeach
                </table>
                @endif
            </div>
        </div>
    </div>
</div>
@stop
