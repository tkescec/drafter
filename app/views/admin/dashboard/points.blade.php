@extends ('admin._layouts.points')

@section('main')

<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-10">
            <h1 style="margin:0!important;">Players Points</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ URL::route('admin.dashboard') }}" class="form-field__input_submit cta-button black">Back to Dashboard</a>
        </div>
    </div>
    <hr>
    <div class="row">
        <div class="col-md-12">
            <ol>
                <li>
                    Minute se upisuju bez sudačke nadoknade (dakle max je 90 minuta)
                    <ol>
                        <li>Ako igrač izađe iz igre u sudačkoj nadoknadi, upisuješ mu 90 minuta, a igraču koji je ušao upisuješ 2,3 minute (pogledaš na whoscoredu koliko je bilo nadoknade pa upises otprilike)</li>
                        <li>OPREZ- u nekim tekmama se iz raznih razloga igra i po 10 minuta sudačke nadoknade, u tom slučaju igraču koji je ušao pred kraj utakmice dodaješ tu sudačku nadoknadu pod minute</li>
                    </ol>
                </li>
                <li>
                    Ako igrač dobije 2 žuta kartona (i time automatski crveni) upisujes mu žute 0, crvene 1
                    <ol>
                        <li>Ako igrač dobije žuti, pa nakon toga izravni crveni, upisuješ mu žuti 1, crveni 1</li>
                    </ol>
                </li>
                <li>OPREZ- gledati acc. crosses</li>
                <li>Ako je igrač u bilo kojem trenu utakmice bio uigri (cak i samo 1 minutu) pod WINNING TEAM mu upisuješ 1 (naravno ako je njegov tim pobijedio)</li>
                <li>CLEEN SHEET upisujes jednako kao i winning team, ako je igrac igrao barem minutu, a njegov tim nije primio gol za vrijeme utakmice, upisujes mu cleen sheet 1</li>
                <li>RECIEVED GOAL upisujes samo za golove koje je ekipa primila dok je određeni igrač bio u polu (ak je izasao na poluvremenu, dok su gubili 1:0, a u drugom su primili još 3 gola, pod rec. goal mu upisujes 1)</li>
                <li>Shot ON TARGET</li>
            </ol>
        </div>
    </div>
    <hr>
    <div class="row" style="margin-bottom:20px; text-align: center;">
        <div class="col-md-2" style="font-size:20px;">
            Position color:
        </div>
        <div class="col-md-1" style="background:rgb(197,217,241);padding:5px;">
            All Position
        </div>
        <div class="col-md-1" style="background:rgb(196,189,151);padding:5px;">
            Goalkeaper
        </div>
        <div class="col-md-1" style="background:rgb(177,160,199);padding:5px;">
            Defender
        </div>
        <div class="col-md-1" style="background:rgb(242,220,219);padding:5px;">
            Midfielder
        </div>
        <div class="col-md-1" style="background:rgb(216,228,188);padding:5px;">
            Forwarder
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            @include('bones-flash::bones.flash')
            @if($errors->any())
            <div class="alert alert-danger" role="alert">
                {{ implode('', $errors->all('<li class="error">:message</li>')) }}
            </div>
            @endif
            <div  style="border:none;">
                <table class="table player-form">
                    <tr style="font-weight:bold;">
                        <th>Player ID</th>
                        <th style="width:250px;">Name</th>
                        <th>Club</th>
                        <th>Minutes</th>
                        <th>Yellow card</th>
                        <th>Red card</th>
                        <th>Own goal</th>
                        <th>Assist</th>
                        <th>Missed penalty</th>
                        <th>Accurate cross</th>
                        <th>Winning team</th>
                        <th>Goal Scored</th>
                        <th>Pass</th>
                        <th>Shot on target</th>
                        <th>Touches</th>
                        <th>Offside</th>
                        <th>Interception</th>
                        <th>Cleareance</th>
                        <th>Dribling</th>
                        <th>Clean sheet</th>
                        <th>Block</th>
                        <th>Goal received</th>
                        <th>Save</th>
                        <th>Penalty saved</th>
                        <th>Claim</th>
                        <th>First goal</th>
                        <th>Other goals</th>
                        <th>Overall</th>
                        <th>Action</th>
                    </tr>
                    @foreach ($data as $player)
                    @if($player['added'] === NULL)
                    {{ Form::open(array('method' => 'POST', 'action' => array('admin.dashboard.points.store', $league_id), 'class'=>'standalone-form', 'style' => 'margin:0 auto')) }}
                    <tr>
                        <td>{{$player['id']}}{{Form::hidden('player_id',$player['id'])}}</td>
                        <td>{{$player['name']}}  <span style="color: #ac2925"> ({{$player['position']}})</span></td>
                        <td>{{$player['club']}}</td>
                        <td>{{ Form::text('minutes', '', array('placeholder' => 'Minutes','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('yellow-card', '', array('placeholder' => 'Yellow card','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('red-card', '', array('placeholder' => 'Red card','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('own-goal', '', array('placeholder' => 'Own goal','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('assist', '', array('placeholder' => 'Assist','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('missed-penalty', '', array('placeholder' => 'Missed penalty','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('accurate-cross', '', array('placeholder' => 'Accurate cross','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('winning-team', '', array('placeholder' => 'Winning team','class' => 'points-all')) }}</td>
                        <td>{{ Form::text('goal-scored', '', array('placeholder' => 'Goal Scored','class' => 'points-all')) }}</td>

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('pass-gk', '', array('placeholder' => 'Pass','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('pass-df', '', array('placeholder' => 'Pass','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('pass-mf', '', array('placeholder' => 'Pass','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('pass-at', '', array('placeholder' => 'Pass','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('shot-gk', '', array('placeholder' => 'Shot on target','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('shot-df', '', array('placeholder' => 'Shot on target','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('shot-mf', '', array('placeholder' => 'Shot on target','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('shot-at', '', array('placeholder' => 'Shot on target','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('touches-gk', '', array('placeholder' => 'Touches','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('touches-df', '', array('placeholder' => 'Touches','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('touches-mf', '', array('placeholder' => 'Touches','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('touches-at', '', array('placeholder' => 'Touches','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('offside-gk', '', array('placeholder' => 'Offside','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('offside-df', '', array('placeholder' => 'Offside','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('offside-mf', '', array('placeholder' => 'Offside','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('offside-at', '', array('placeholder' => 'Offside','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('interception-gk', '', array('placeholder' => 'Interception','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('interception-df', '', array('placeholder' => 'Interception','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('interception-mf', '', array('placeholder' => 'Interception','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('interception-at', '', array('placeholder' => 'Interception','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('cleareance-gk', '', array('placeholder' => 'Cleareance','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('cleareance-df', '', array('placeholder' => 'Cleareance','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('cleareance-mf', '', array('placeholder' => 'Cleareance','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('cleareance-at', '', array('placeholder' => 'Cleareance','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('dribling-gk', '', array('placeholder' => 'Dribling','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('dribling-df', '', array('placeholder' => 'Dribling','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('dribling-mf', '', array('placeholder' => 'Dribling','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('dribling-at', '', array('placeholder' => 'Dribling','class' => 'points-at')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('clean-sheet-gk', '', array('placeholder' => 'Clean sheet','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('clean-sheet-df', '', array('placeholder' => 'Clean sheet','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('clean-sheet-mf', '', array('placeholder' => 'Clean sheet','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('clean-sheet-at', '', array('placeholder' => 'Clean sheet','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('block-gk', '', array('placeholder' => 'Block','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('block-df', '', array('placeholder' => 'Block','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('block-mf', '', array('placeholder' => 'Block','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('block-at', '', array('placeholder' => 'Block','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('goal-received-gk', '', array('placeholder' => 'Goal received','class' => 'points-gk', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('goal-received-df', '', array('placeholder' => 'Goal received','class' => 'points-df')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('goal-received-mf', '', array('placeholder' => 'Goal received','class' => 'points-mf')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('goal-received-at', '', array('placeholder' => 'Goal received','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('save-gk', '', array('placeholder' => 'Save','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('save-df', '', array('placeholder' => 'Save','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('save-mf', '', array('placeholder' => 'Save','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('save-at', '', array('placeholder' => 'Save','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('penalty-saved-gk', '', array('placeholder' => 'Penalty saved','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('penalty-saved-df', '', array('placeholder' => 'Penalty saved','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('penalty-saved-mf', '', array('placeholder' => 'Penalty saved','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('penalty-saved-at', '', array('placeholder' => 'Penalty saved','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('claim-gk', '', array('placeholder' => 'Claim','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('claim-df', '', array('placeholder' => 'Claim','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('claim-mf', '', array('placeholder' => 'Claim','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('claim-at', '', array('placeholder' => 'Claim','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('first-goal-gk', '', array('placeholder' => 'First goal','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('first-goal-df', '', array('placeholder' => 'First goal','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('first-goal-mf', '', array('placeholder' => 'First goal','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('first-goal-at', '', array('placeholder' => 'First goal','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        @if($player['position'] == 'gk')
                        <td>{{ Form::text('other-goals-gk', '', array('placeholder' => 'Other goals','class' => 'points-gk')) }}</td>
                        @endif
                        @if($player['position'] == 'df')
                        <td>{{ Form::text('other-goals-df', '', array('placeholder' => 'Other goals','class' => 'points-df', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'mf')
                        <td>{{ Form::text('other-goals-mf', '', array('placeholder' => 'Other goals','class' => 'points-mf', 'disabled')) }}</td>
                        @endif
                        @if($player['position'] == 'at')
                        <td>{{ Form::text('other-goals-at', '', array('placeholder' => 'Other goals','class' => 'points-at', 'disabled')) }}</td>
                        @endif

                        <td></td>
                        <td>{{ Form::submit('Save', array('class' => '')) }}</td>

                    </tr>
                    {{ Form::close() }}
                    @else
                    <tr>
                        <td>{{$player['id']}}{{Form::hidden('player_id',$player['id'])}}</td>
                        <td>{{$player['name']}}  <span style="color: #ac2925"> ({{$player['position']}})</span></td>
                        <td>{{$player['club']}}</td>
                        <td>{{$player['minutes']}} <span style="color: #39ac20"> (Min)</span></td>
                        <td>{{$player['yellow_card']}} <span style="color: #39ac20"> (Y.card)</span></td>
                        <td>{{$player['red_card']}} <span style="color: #39ac20"> (R.card)</span></td>
                        <td>{{$player['own_goal']}} <span style="color: #39ac20"> (O.goal)</span></td>
                        <td>{{$player['asist']}} <span style="color: #39ac20"> (Assist)</span></td>
                        <td>{{$player['missed_penalty']}} <span style="color: #39ac20"> (M.penalty)</span></td>
                        <td>{{$player['accurate_cross']}} <span style="color: #39ac20"> (A.cross)</span></td>
                        <td>{{$player['winning_team']}} <span style="color: #39ac20"> (W.team)</span></td>
                        <td>{{$player['goal']}} <span style="color: #39ac20"> (G.scored)</span></td>
                        <td>{{$player['pass']}} <span style="color: #39ac20"> (Pass)</span></td>
                        <td>{{$player['shot_on_target']}} <span style="color: #39ac20"> (S.target)</span></td>
                        <td>{{$player['touches']}} <span style="color: #39ac20"> (Touches)</span></td>
                        <td>{{$player['offside']}} <span style="color: #39ac20"> (Offside)</span></td>
                        <td>{{$player['interception']}} <span style="color: #39ac20"> (Interception)</span></td>
                        <td>{{$player['cleareance']}} <span style="color: #39ac20"> (Cleareance)</span></td>
                        <td>{{$player['dribling']}} <span style="color: #39ac20"> (Dribbling)</span></td>
                        <td>{{$player['clean_sheet']}} <span style="color: #39ac20"> (C.sheet)</span></td>
                        <td>{{$player['block']}} <span style="color: #39ac20"> (Block)</span></td>
                        <td>{{$player['rec_goal']}} <span style="color: #39ac20"> (R.goal)</span></td>
                        <td>{{$player['save']}} <span style="color: #39ac20"> (Save)</span></td>
                        <td>{{$player['penalty_saved']}} <span style="color: #39ac20"> (P.saved)</span></td>
                        <td>{{$player['claim']}} <span style="color: #39ac20"> (Claim)</span></td>
                        <td>{{$player['first_rec_goal']}} <span style="color: #39ac20"> (1.goal)</span></td>
                        <td>{{$player['other_rec_goal']}} <span style="color: #39ac20"> (Ot.goal)</span></td>
                        <td>{{$player['points']}}</td>
                        <td><a href="{{ URL::route('admin.dashboard.points.edit', array('league_id'=>$league_id,'id'=>$player['id'])) }}">Reset</a></td>
                    </tr>
                    @endif
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</div>

@stop
