<header class="main-header">
    <a href="{{ URL::route('/') }}" class="main-header__logo">Drafter</a>
    <nav class="main-header__navigation">
        <a href="{{ URL::route('user.dashboard') }}" class="main-header__navigation_item">Dashboard</a>
        <a href="#" class="main-header__navigation_item leagues">Play Leagues</a>
        @if(! Sentry::check())
        <a href="{{ URL::route('auth.login') }}" class="main-header__navigation_item">
            Login
        </a>
        @else
        <a href="#" class="main-header__navigation_item">
            {{ $user = Sentry::getUser()->first_name; }}
        </a>
        @endif
    </nav>
</header>