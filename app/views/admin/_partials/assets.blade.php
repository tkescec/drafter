<!--############ GOOGLE FONTS #################-->
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin,latin-ext' rel='stylesheet'>
<!--############ FONT AWESOME #################-->
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet'>
<!--############ CSS FILES #################-->
<link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/my-style.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/datepicker3.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/jquery.timepicker.css') }}" rel="stylesheet">

<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--############ JQUERY LIBRARY #################-->
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<!--############ STICKY HEADER #################-->
<script src="{{ URL::asset('assets/js/StickyHeader.js') }}"></script>
<!--############ BOOTSTRAP DATE & TIME #################-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<script src="{{ URL::asset('assets/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery.timepicker.min.js') }}"></script>
<script src="{{ URL::asset('assets/js/jquery-ias.min.js') }}"></script>

