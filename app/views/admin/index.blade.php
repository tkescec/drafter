@extends ('admin._layouts.index')

@section('main')

{{ Form::open(array('id'=>'admin-form', 'class'=>'standalone-form')) }}
@include('bones-flash::bones.flash')
@if($errors->any())
<div class="alert alert-danger" role="alert">
    {{ implode('', $errors->all('<li class="error">:message</li>')) }}
</div>
@endif
<fieldset>
    <div class="standalone-form__form-field">
        {{ Form::email('email', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter email')) }}
    </div>
    <div class="standalone-form__form-field">
        {{ Form::password('password', array('class' => 'form-field__input_text', 'placeholder' => 'Enter password')) }}
    </div>
    <div class="standalone-form__form-field">
        <label class="form-field__label">
            {{ Form::checkbox('remember', '', array('class' => 'form-field__input_checkbox')) }}
            Remember me for 2 weeks
        </label>
    </div>
    <div class="standalone-form__form-field">
        {{ Form::submit('Login', array('class' => 'form-field__input_submit cta-button green')) }}
        <!-- <a href="dashboard.php" class="form-field__input_submit cta-button green">Login</a>

         <input type="submit" class="form-field__input_submit cta-button" placeholder="Login" value="login">
         -->
    </div>
</fieldset>
{{ Form::close() }}
@stop