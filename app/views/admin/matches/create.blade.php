@extends ('admin._layouts.matches')

@section('main')
<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-10">
            <h1 style="margin:0!important;">Add New Match</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ URL::route('admin.dashboard') }}" class="form-field__input_submit cta-button black">Back to Dashboard</a>
        </div>
    </div>
    <hr>
    {{ Form::open(array('route' => 'admin.matches.store','id'=>'matches-form', 'class'=>'standalone-form', 'style' => 'margin:0 auto','files'=>true)) }}
    @include('bones-flash::bones.flash')
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </div>
    @endif
    <fieldset>
        {{Form::hidden('league_id',$league_id)}}
        <div class="standalone-form__form-field">
            {{Form::label('home', 'Home')}}
            {{Form::select('home', $clubs, '', array('class' => 'form-field__input_text'))}}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('away', 'Away')}}
            {{Form::select('away', $clubs, '', array('class' => 'form-field__input_text'))}}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('match-date', 'Match Date')}}
            {{ Form::text('match-date', '', array('class' => 'form-field__input_text', 'placeholder' => 'Match Date', 'id'=>'match-date')) }}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('match-time', 'Match Time')}}
            {{ Form::text('match-time', '', array('class' => 'form-field__input_text', 'placeholder' => 'Match Time', 'id'=>'match-time')) }}
        </div>
        <div class="standalone-form__form-field clearfix">
            {{ Form::submit('Add', array('class' => 'form-field__input_submit cta-button green left')) }}
            <a href="{{URL::route('admin.dashboard')}}" class="form-field__input_submit cta-button btn-danger right">Cancle</a>
        </div>
    </fieldset>
    {{ Form::close() }}
    <div class="row">
        <div class="col-md-12">
            <div class="table-responsive">
                <hr>
                <h1>Added Matches</h1>
                <hr>
                @if ($matches->count() == 0)
                <hr>
                <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no matches for this league.</p>
                @endif
                @if ($matches->count() > 0)
                <table class="table table-hover matches">
                    <tr style="font-weight: bold;">
                        <th style="text-align: center;">Home</th>
                        <th style="text-align: center;"></th>
                        <th style="text-align: center;">Guest</th>
                        <th style="text-align: center;">Date</th>
                        <th style="text-align: center;">Time</th>
                        <th style="text-align: center;">Action</th>
                    </tr>
                    @foreach ($matches as $match)
                    <?php
                    $d = explode('-',$match->date);
                    $f = $d[2].'/'.$d[1].'/'.$d[0];
                    $time_array = explode(':',$match->time);
                    $time = $time_array[0].':'.$time_array[1];
                    ?>
                    <tr>
                        <td style="text-align: center;">{{$match->club1}}</td>
                        <td style="text-align: center;">-</td>
                        <td style="text-align: center;">{{$match->club2}}</td>
                        <td style="text-align: center;">{{$f}}</td>
                        <td style="text-align: center;">{{$time}}</td>
                        <td style="text-align: center;">
                            <a href="{{ URL::route('admin.matches.edit', $match->id) }}" data-toggle="tooltip" title="Edit match">
                                <i class="fa fa-pencil"></i>
                            </a>&nbsp;
                            <a href="#" data-toggle="modal" data-target="#delete{{$match->id;}}">
                                <i class="fa fa-trash-o" data-toggle="tooltip" title="Delete match"></i>
                            </a>
                        </td>
                    </tr>
                    <!------------------ Delete league dialog---------------->
                    @if ($match->count() > 0)
                    <div class="modal fade" id="delete{{$match->id;}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" style="text-align:center;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">warning!</h2>
                                </div>
                                <div class="modal-body">
                                    <h5>Are you sure you want to delete a <b>&quot;{{ $match->club1 }}&quot; vs &quot;{{ $match->club2 }}&quot;</b> match?</h5>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal" style="margin-right:10px;">No</button>
                                    {{ Form::open(array('method' => 'DELETE', 'action' => array('admin.matches.destroy', $match->id), 'class' => 'pull-right')) }}
                                    {{ Form::submit('Yes , I&rsquo;m sure', array('class' => 'btn btn-danger')) }}
                                    {{ Form::close() }}
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    <!------------------ End Delete tournament dialog---------------->
                    @endforeach
                    @endif
                </table>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $('#match-date').datepicker({
            format: "dd/mm/yyyy"
        });
        $('#match-time').timepicker({
            'timeFormat': 'H:i' ,
            'step': 15
        });
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>
@stop
