@extends ('admin._layouts.matches')

@section('main')
<?php
$date = $match->date;
$explodedate = explode('-', $date);
$newdate = $explodedate[2].'/'.$explodedate[1].'/'.$explodedate[0];
?>
<div class="container-fluid">
    <hr>
    <div class="row" >
        <div class="col-md-10">
            <h1 style="margin:0!important;">Edit {{$match->club1}} vs {{$match->club2}} Match</h1>
        </div>
        <div class="col-md-2">
            <a href="{{ URL::route('admin.dashboard') }}" class="form-field__input_submit cta-button black">Back to Dashboard</a>
        </div>
    </div>
    <hr>
    {{ Form::open(array('method' => 'PUT', 'action' => array('admin.matches.update', $match->id),'id'=>'matches-form', 'class'=>'standalone-form', 'style' => 'margin:0 auto')) }}
    @include('bones-flash::bones.flash')
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </div>
    @endif
    <fieldset>
        {{Form::hidden('league_id',$league_id)}}
        <div class="standalone-form__form-field">
            {{Form::label('home', 'Home')}}
            {{Form::select('home', $clubs, $match->club1, array('class' => 'form-field__input_text'))}}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('away', 'Away')}}
            {{Form::select('away', $clubs, $match->club2, array('class' => 'form-field__input_text'))}}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('match-date', 'Match Date')}}
            {{ Form::text('match-date', $newdate, array('class' => 'form-field__input_text', 'placeholder' => 'Match Date', 'id'=>'match-date')) }}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('match-time', 'Match Time')}}
            {{ Form::text('match-time', $match->time, array('class' => 'form-field__input_text', 'placeholder' => 'Match Time', 'id'=>'match-time')) }}
        </div>
        <div class="standalone-form__form-field clearfix">
            {{ Form::submit('Save', array('class' => 'form-field__input_submit cta-button green left')) }}
            <a href="{{URL::route('admin.matches.create',$league_id)}}" class="form-field__input_submit cta-button btn-danger right">Cancle</a>
        </div>
    </fieldset>
    {{ Form::close() }}
</div>
<script>
    $(document).ready(function() {
        $('#match-date').datepicker({
            format: "dd/mm/yyyy"
        });
        $('#match-time').timepicker({
            'timeFormat': 'H:i' ,
            'step': 15
        });
    });
</script>
@stop
