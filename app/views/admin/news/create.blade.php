@extends ('admin._layouts.news')

@section('main')
<div class="container-fluid">
    <hr>
    <h1>Create New News</h1>
    <hr>
    {{ Form::open(array('route' => 'admin.news.store','id'=>'news-form', 'class'=>'standalone-form', 'style' => 'margin:0 auto')) }}
    @include('bones-flash::bones.flash')
    @if($errors->any())
    <div class="alert alert-danger" role="alert">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        {{ implode('', $errors->all('<li class="error">:message</li>')) }}
    </div>
    @endif
    <fieldset>

        <div class="standalone-form__form-field">
            {{Form::label('news-title', 'Title')}}
            {{ Form::text('news-title', '', array('class' => 'form-field__input_text', 'placeholder' => 'News Title')) }}
        </div>
        <div class="standalone-form__form-field">
            {{Form::label('news-content', 'Content')}}
            {{ Form::textarea('news-content', '', array('size' => '45x9','class' => 'form-field__input_text', 'placeholder' => 'News Content')) }}
        </div>
        <div class="standalone-form__form-field clearfix">
            {{ Form::submit('Create', array('class' => 'form-field__input_submit cta-button green left')) }}
            <a href="{{URL::route('admin.dashboard')}}" class="form-field__input_submit cta-button btn-danger right">Cancle</a>
        </div>
    </fieldset>
    {{ Form::close() }}
</div>
@stop
