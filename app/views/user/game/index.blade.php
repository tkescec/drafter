@extends ('_layouts.editdraft')

@section('main')
@foreach ($games as $game)
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=971323262892342&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>
<section class="content-wrap draft">
    <div id="sticker" class="leagues-listing__item draft-headline">
        <div class="leagues-listing__item_name" id="league_title">{{($game->leagues->title)}}</div>
        <div class="leagues-listing__item_block_wrapper">
            <div class="leagues-listing__item_block players">
                <small class="leagues-listing__item_label">no od players</small>
                <span class="leagues-listing__item_value">{{ $game->leagues->active_players }}/{{ $game->leagues->num_players }}</span>
            </div>
            <div class="leagues-listing__item_block entry-fee">
                <small class="leagues-listing__item_label">entry-fee</small>
                <span class="leagues-listing__item_value">{{ $game->leagues->entry_fee }}</span>
            </div>
            <div class="leagues-listing__item_block salary-cap">
                <small class="leagues-listing__item_label">salary cap</small>
                <span class="leagues-listing__item_value" salary-cap="{{ $game->leagues->salary_cup }}" data-trigger="click"data-placement="bottom" data-container="body" data-toggle="popover" title="You don't have enough fund to add this player">€{{ round($game->leagues->salary_cup,2) }}M</span>
            </div>
            <div class="leagues-listing__item_block time">
                <small class="leagues-listing__item_label">time left</small>
                <span class="leagues-listing__item_value" id="field{{ $game->leagues->id }}">00 days 00:00:00</span>
            </div>
        </div>
        <div class="draft-headline_cta" style="width:510px">
            <div class="cta"><a href="" class="cta-button outline challenge" data-toggle="modal" data-target="#challenge_friend" id="challenge">challenge friend</a></div>
            <div class="cta"><a href="{{URL::route('user.dashboard')}}" class="cta-button black" style="width:auto;">cancel</a></div>
            <div class="cta"><a href="#" class="cta-button green" id="save" style="width:auto;">save</a></div>
        </div>
        <div class="clear"></div>
    </div>
    <div class="future-games">
        <h2 class="future-games_headline">Upcoming matches</h2>
        <div class="box-outer">
            <div class="box-inner">
                @foreach ($matches as $match)
                <?php
                $date = explode('-',$match->date);
                $year = $date[0];
                $month = $date[1];
                $day = $date[2];
                $time = explode(':',$match->time);
                $h = $time[0];
                $m = $time[1];
                $date_matches = $match->date;
                $time_matches = $match->time;
                $timestamp = strtotime($date_matches.' '. $time_matches);
                $current_time = time()+7200;
                ?>
                @if($current_time < $timestamp)
                <a href="#" class="future-games__item">
                    <span class="future-games__item_timestamp">{{$day.'/'.$month.'/'.$year}} {{$h.':'.$m}}</span>
                    <span class="future-games__item_label">{{$match->club1}} - {{$match->club2}}</span>
                </a>
                @endif
                @endforeach
            </div>
        </div>
    </div>
    <div class="field-wrap">
        <div class="field 3-4-4">
            <div id="formation-fields"></div>
            <img src="{{ URL::asset('assets/images/layout/football-field.jpg')}}" alt="">
        </div>
    </div>
    <div class="tooltray">
        <form action="/" class="formation-module">
            <fieldset>
                <label for="formation" class="tooltray__label">Select formation</label>
                <div class="formation-module__selector">
                    <a class="formation-module__selector_item" data-for='3-4-3'>3-4-3</a>
                    <a class="formation-module__selector_item" data-for='4-4-2'>4-4-2</a>
                    <a class="formation-module__selector_item" data-for='4-3-3'>4-3-3</a>
                </div>
            </fieldset>
        </form>
    </div>
    <div class="players-select-wrap">
        <div class="player-filter-tray">
            <div class="player-filter__search">
                <input type="text" value="" class="player-filter__search_input-field" placeholder="search for players"id="autocomplete">
                <input type="submit" class="player-filter__search_input-button">
                <div id="select"></div>
            </div>
            <form action="/" class="player-filter">
                <fieldset class="player-filter__position">
                    <label for="position" class="player-filter__label">Player position</label>
                    <ul class="player-filter__position_buttons">
                        <li><a class="player-filter__position_buttons_item all active">all</a></li>
                        <li><a class="player-filter__position_buttons_item gk">gk</a></li>
                        <li><a class="player-filter__position_buttons_item df">df</a></li>
                        <li><a class="player-filter__position_buttons_item mf">mf</a></li>
                        <li><a class="player-filter__position_buttons_item at">at</a></li>
                    </ul>
                </fieldset>
                <div>
                    <fieldset class="player-filter__club">
                        <select name="Club" id="club-filter" class="player-filter__select">
                            <option value="all">All Clubs</option>
                            @foreach ($players as $player)
                            <?php $clubs[] = $player->club; ?>
                            @endforeach
                            <?php $clubs = array_unique($clubs);
                            asort($clubs); ?>
                            @foreach ($clubs as $club)
                            <option value="{{$club}}">{{$club}}</option>
                            @endforeach
                        </select>
                    </fieldset>
                    <fieldset class="player-filter__price">
                        <label for="position" class="player-filter__label">Price range</label>
                        <b>€ 0</b><input id="ex2" type="text" class="span2" value="" data-slider-min="0"data-slider-max="100" data-slider-step="1" data-slider-value="[0,100]"/><b>€100M</b>
                    </fieldset>
                </div>
                <a href="#" class="reset-filter">Reset filters</a>
            </form>
            <div class="clear"></div>
        </div>
        <div class="players-wrap" id="horiz_container_outer">
            <div class="players-wrap__inner" id="horiz_container_inner">
                <div class="preloader">
                    <img src="{{ URL::asset('assets/images/preloader/328.gif')}}">
                </div>
                <div id="horiz_container">
                    @foreach ($players as $player)
                    <div class="player-card" id="{{$player->id}}" data-draft='null'>
                        <div class="player-card__selector"></div>
                        <div class="player-card__avatar"><img src="{{$player->image}}" alt=""></div>
                        <div class="player-card__name">{{$player->name}}</div>
                        <div class="player-card__club">{{$player->club}}</div>
                        <div class="player-card__games-played">
                            <span class="player-card__info_label">position:</span>
                            <span class="player-card__info_value">{{$player->position}}</span>
                        </div>
                        <div class="player-card__price" price="{{round($player->value,2)}}">€{{round($player->value,2)}}M</div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>

<!------------------ Player selector dialog ---------------->
<div class="modal fade" id="formation_change" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel"style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">Warning!</h2>
            </div>
            <div class="modal-body">
                If you change your formation , all the players in the field will be deleted.
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default closex" style="margin-right:10px;">No</button>
                <button type="button" class="btn btn-danger change_formation" data-dismiss="modal"style="margin-right:10px;">Yes</button>
            </div>
        </div>
    </div>
</div>
<!------------------ End Player selector dialog---------------->
<!------------------ Challenge friend dialog ---------------->
<div class="modal fade" id="challenge_friend" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel"style="color:#2ba815;text-transform:uppercase;font-size:1.6rem;">challenge friend</h2>
            </div>
            <div class="modal-body">
                <button type="button" class="btn btn-default email-invite" ><i class="fa fa-envelope fa-lg"></i> Invite by email</button>
            </div>
            <div class="modal-footer">
                <div class="email-invite">
                    {{ Form::open(array('id'=>'invite-form', 'class'=>'standalone-form','style' => 'margin:0 auto')) }}
                    <fieldset>
                        @if(! Sentry::check())
                        <div class="standalone-form__form-field">
                            {{ Form::text('nickname', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter your name')) }}
                        </div>
                        @endif
                        <div class="standalone-form__form-field">
                            {{ Form::text('friend-name', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter friend name')) }}
                        </div>
                        <div class="standalone-form__form-field">
                            {{ Form::email('email', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter email')) }}
                        </div>
                        <div class="standalone-form__form-field">
                            <a href="#" class="cta-button green" id="send-invitation">Send Invitation</a>
                        </div>
                    </fieldset>
                    {{ Form::close() }}
                </div>
                <div class="facebook-invite">

                </div>
            </div>
        </div>
    </div>
</div>
<!------------------ End challenge friend dialog---------------->

<div class="modal fade" id="wrongposition" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content" style="text-align:center;padding:20px;">
            Wrong Position!
        </div>
    </div>
</div>

<div class="modal fade" id="fillfield" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-fill">
        <div class="modal-content" style="text-align:center;padding:20px;">
            Please fill in all fields in order to continue.
        </div>
    </div>
</div>

<div class="modal fade" id="nofound" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-fill">
        <div class="modal-content" style="text-align:center;padding:20px;">
            You don't have enough money on your salary cap.
        </div>
    </div>
</div>
<script>

function counter(field, time, button) {

    // access to html element
    var counter_field = document.getElementById(field);
    var button_field = document.getElementById(button);
    var challenge = document.getElementById("challenge");
    var fb_share = document.getElementById("fb_share");

    // destination date/time
    var date_stop = time; //new Date(2013,6,7,0,0,0,0);

    // call counting function every second (1000 ms)
    var counting_loop = setInterval(counting, 1000);

    // counting function
    function counting()
    {
        // current date/time
        var date_now = new Date();

        if( date_stop == null ) {
            var text = "- you forget date/time -";
        } else {

            // convert miliseconds to seconds
            var diff = Math.round((date_stop-date_now)/1000);

            // still in future
            if( (0 < diff) )
            {
                // convert diff to days, hours, minutes, seconds

                // seconds
                var seconds = diff % 60;
                diff = (diff-seconds) / 60;
                if( seconds < 10 ) seconds = "0" + seconds;

                // minutes
                var minutes = diff % 60;
                diff = (diff-minutes) / 60;
                if( minutes < 10 ) minutes = "0" + minutes;

                // hours
                var hours = diff % 24;
                diff = (diff-hours) / 24;
                if( hours < 10 ) hours = "0" + hours;

                // days
                var days = diff;
                if( days < 10 ) days = "0" + days;

                // convert to text
                text = days+" days " +hours+":"+minutes+":"+seconds;
            } else {
                // removing counter
                button_field.classList.add("disabled");
                button_field.removeAttribute("href");
                fb_share.classList.add("disabled");
                fb_share.removeAttribute("href");
                challenge.classList.add("disabled");
                challenge.removeAttribute("data-target");
                counter_field.innerHTML = "THE END";
                unsetInterval(counting_loop);

            }
        }
        // put text into html
        counter_field.innerHTML = text;
    }
}
window.onload = function () {
    <?php
    $date = explode('-',$game->leagues->date);
    $year = $date[0];
    $month = $date[1] - 1;
    $day = $date[2];
    $time = explode(':',$game->leagues->time);
    $hours = $time[0];
    $minutes = $time[1];
    $seconds = $time[2];
    ?>

    counter("field{{ $game->leagues->id }}", new Date({{$year}}, {{$month}},{{$day}},{{$hours}},{{$minutes}},{{$seconds}},0),"save");}

$(document).ready(function(){

    var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8});

    //players formation storage

    storage = {{$game->pif}};
    console.log(storage);

    //game id
    game_id = '{{$game->id}}';

    //user info
    user_info = '{{$user}}';

    //league info
    league_id = '{{ $game->leagues->id }}';
    league_name = '{{ $game->leagues->title }}';
    league_type = '{{ $game->leagues->type }}';
    email_open = false;
    facebook_open = false;
    $('#challenge_friend .modal-body').append('<a href="http://www.facebook.com/dialog/send?app_id=971323262892342&link=http://www.playdrafter.com/draft/'+league_id+'&display=page&redirect_uri=http://www.playdrafter.com/draft/'+league_id+'" type="button" class="btn btn-primary facebook-invite" target="_blank"><i class="fa fa-facebook-square fa-lg"></i> Invite by facebook</a>');
    $('.draft-headline_cta').prepend('<div class="cta"><a href="https://www.facebook.com/dialog/share?app_id=971323262892342&display=touch&href=http://playdrafter.com/draft/'+league_id+'&redirect_uri=http://playdrafter.com/draft/'+league_id+'" class="cta-button outline" id="fb_share">share on facebook</a></div>');

    //invite friends
    $('body').on('click','.challenge',function(){
        $('div.email-invite').hide();
        $('div.facebook-invite').hide();
        email_open = false;
        facebook_open = false;
    })
    $('body').on('click','button.email-invite',function(){
        if(facebook_open === true){
            $('div.facebook-invite').slideUp(200,function(){
                $('div.email-invite').slideDown(200);
                email_open = true;
            });
        }
        else{
            $('div.email-invite').slideDown(200);
            email_open = true;
        }

    });
    $('body').on('click','button.facebook-invite',function(){
        if(email_open === true){
            $('div.email-invite').slideUp(200,function(){
                $('div.facebook-invite').slideDown(200);
                facebook_open = true;
            });
        }
        else{
            $('div.facebook-invite').slideDown(200);
            facebook_open = true;
        }
    });

    $('body').on('click', '#send-invitation', function(){
        if($('#invite-form input[name=nickname]').length == 0){
            email = $('#invite-form input[name=email]').val();
            friend_name = $('#invite-form input[name=friend-name]').val();
            $('#invite-form').prepend('<div class="preload" style="padding:10px 0;margin:0;text-align:center;"><img src="{{ URL::asset("assets/images/preloader/328.gif")}}"></div>');
            if(email == '' || friend_name == ''){
                $('.preload').remove();
                $('.alert-danger').remove();
                $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter email address and friend name.</div>');
                $('.alert-danger').delay(2000).slideUp();
            }
            else{
                if(isValidEmailAddress(email)){
                    user = JSON.parse(user_info);
                    user = $.makeArray( user );
                    $.map(user, function(key){
                        user_name = (key.nickname);
                    });
                    data = 'league_id='+league_id+'&email='+email+'&user_name='+user_name+'&friend_name='+friend_name+'&league_name='+league_name;
                    $.ajax({
                        type: "POST",
                        url: "{{URL::to('draft/invite')}}",
                        data: data,
                        cache: true,
                        success: function (result) {
                            $('.preload').remove();
                            if (result == 'sent') {
                                $('#invite-form').prepend('<div class="alert alert-success" role="alert" style="text-align:center">Your email has been sent!</div>');
                                $('.alert-success').delay(2000).slideUp();
                            }
                            else {
                                $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Something went wrong. Please try again.</div>');
                                $('.alert-danger').delay(2000).slideUp();
                            }
                        }
                    }, "json");
                }
                else{
                    $('.preload').remove();
                    $('.alert-danger').remove();
                    $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter correct email address.</div>');
                    $('.alert-danger').delay(2000).slideUp();
                }
            }
        }
        else{
            email = $('#invite-form input[name=email]').val();
            nickname = $('#invite-form input[name=nickname]').val();
            friend_name = $('#invite-form input[name=friend-name]').val();
            $('#invite-form').prepend('<div class="preload" style="padding:10px 0;margin:0;text-align:center;"><img src="{{ URL::asset("assets/images/preloader/328.gif")}}"></div>');
            if(email == '' || nickname == '' || friend_name == ''){
                $('.preload').remove();
                $('.alert-danger').remove();
                $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter email address, your name and friend name.</div>');
                $('.alert-danger').delay(2000).slideUp();
            }
            else{
                if(isValidEmailAddress(email)){
                    data = 'league_id='+league_id+'&email='+email+'&user_name='+nickname+'&friend_name='+friend_name+'&league_name='+league_name;
                    $.ajax({
                        type: "POST",
                        url: "{{URL::to('draft/invite')}}",
                        data: data,
                        cache: true,
                        success: function (result) {
                            $('.preload').remove();
                            if (result == 'sent') {
                                $('#invite-form').prepend('<div class="alert alert-success" role="alert" style="text-align:center">Your email has been sent!</div>');
                                $('.alert-success').delay(2000).slideUp();
                            }
                            else {
                                $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Something went wrong. Please try again.</div>');
                                $('.alert-danger').delay(2000).slideUp();
                            }
                        }
                    }, "json");
                }
                else{
                    $('.preload').remove();
                    $('.alert-danger').remove();
                    $('#invite-form').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter correct email address.</div>');
                    $('.alert-danger').delay(2000).slideUp();
                }
            }
        }
    });

    function isValidEmailAddress(emailAddress) {
        var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
        return pattern.test(emailAddress);
    }

    //if game full disable challenge button
    if({{ $game->leagues->active_players }} >= {{ $game->leagues->num_players }}){
        $(".challenge").addClass('disabled');
        $(".challenge").removeAttr('data-target');
        $("#fb_share").addClass('disabled');
        $("#fb_share").removeAttr('href');
    }

    //save game
    $('body').on('click','#save', function () {
        formation_storage = $('.formation-module__selector_item.active').html();
        numItems = $('.drafted').length;
        if ($(this).hasClass('disabled')) {
            // do nothing
        }
        else{
            if(numItems != 11){
                $('#fillfield').modal('show');
            }
            else{
                players = JSON.stringify(storage);
                fund = $('.salary-cap .leagues-listing__item_value').attr('salary-cap');
                data = 'players='+players+'&game_id='+game_id+'&formation='+formation_storage+'&fund='+fund+'&league_id='+league_id;
                $.ajax({
                    type: "PUT",
                    url: "{{URL::to('user/game/update')}}",
                    data: data,
                    cache: true,
                    success: function (result) {
                        if (result == 'yes') {
                            window.location.replace("/user/dashboard");
                        }
                        if (result == 'no') {
                            window.location.replace("/auth/login");
                        }
                        if (result == '') {
                            //error
                        }
                    }
                }, "json");
            }
        }
    });

    // function for set horizontal scroller
    function setScroller(offset) {
        cardWidth = $('.player-card').width();
        numItems = $('.player-card').length;
        containrWidth = (numItems * cardWidth) + offset;
        $('#horiz_container').css('width', containrWidth);
        elementWidth = $('.future-games__item').width();
        numElement = $('.future-games__item').length;
        boxWidth = (numElement * elementWidth) + offset;
        $('.box-inner').css('width', containrWidth);
    }

    // foramtion fields
    formation343 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-3" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-3" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-3" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';
    formation442 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-2" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-2" data-position="at" price="0" id="null" data-pos="at2"></span>';
    formation433 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-3" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-3" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-3" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';

    //set game formation
    switch('{{$game->formation}}'){
        case '3-4-3':
            $('#formation-fields').append(formation343);
            $('.formation-module__selector_item[data-for="3-4-3"]').addClass('active');
            break;
        case '4-3-3':
            $('#formation-fields').append(formation433);
            $('.formation-module__selector_item[data-for="4-3-3"]').addClass('active');
            break;
        case '4-4-2':
            $('#formation-fields').append(formation442);
            $('.formation-module__selector_item[data-for="4-4-2"]').addClass('active');
            break;
    }

    //set salary cap
    $('.salary-cap .leagues-listing__item_value').attr('salary-cap','{{$game->fund_amount}}');
    $('.salary-cap .leagues-listing__item_value').html('€{{round($game->fund_amount,2)}}M');

    // set price range slider
    $("#ex2").bootstrapSlider();

    //change formation
    $('body').on('click', '.formation-module__selector_item', function () {
        field = $(this);
        if ($('.field__player-position').hasClass('drafted')) {
            $('#formation_change').modal('show');
            $('body').one('click', '.change_formation', function () {
                $('.salary-cap .leagues-listing__item_value').tooltip('hide');
                $('.salary-cap .leagues-listing__item_value').css('color', 'black');
                $('.player-card .player-card__selector').removeClass('active');
                $('.field__player-position').removeClass('field-focus');
                $('.player-card').attr('data-draft', 'null');
                $('.player-card .player-card__selector').removeClass('draft');
                $('.salary-cap .leagues-listing__item_value').html('€' + {{$game->leagues->salary_cup}} + 'M');
                $('.salary-cap .leagues-listing__item_value').attr('salary-cap', {{$game->leagues->salary_cup}});
                $('.field__player-position').attr('price', 0);
                $('.field__player-position').attr('id', 'null');
                $('.field__player-position').removeClass('drafted');
                $('.formation-module__selector_item').removeClass('active');
                field.addClass('active');
                $('#formation-fields').empty();
                storage = [];
                salary_cup_storage = [];
                switch (field.html()) {
                    case '3-4-3':
                        $('#formation-fields').append(formation343);
                        break;
                    case '4-3-3':
                        $('#formation-fields').append(formation433);
                        break;
                    case '4-4-2':
                        $('#formation-fields').append(formation442);
                        break;
                }
            });
            $('.close, .closex').click(function () {
                $('#formation_change').modal('hide');
            });

        }
        else{
            $('.formation-module__selector_item').removeClass('active');
            $(this).addClass('active');
            $('#formation-fields').empty();
            switch ($(this).html()) {
                case '3-4-3':
                    $('#formation-fields').append(formation343);
                    break;
                case '4-3-3':
                    $('#formation-fields').append(formation433);
                    break;
                case '4-4-2':
                    $('#formation-fields').append(formation442);
                    break;
            }

        }
    });

    //player card
    $('body').on('click', '.player-card', function () {
        $('.salary-cap .leagues-listing__item_value').tooltip('hide');
        $('.salary-cap .leagues-listing__item_value').css('color', 'black');
        player_card = $(this);
        if ($(this).attr('data-draft') != 'drafted') {
            field_card = '<span class="field__player-position_profile-img"><img src="' + $('div.player-card__avatar img', this).attr('src') + '" alt=""></span><span class="field__player-position_name">' + $('div.player-card__name', this).html() + '<br><span style="color:black;font-family:roboto;">' + $('.player-card__price', this).html() + '</span></span>';
            if ($('.player-card__selector', this).hasClass('active')) {
                $('.player-card__selector', this).removeClass('active');
                $('.field__player-position').removeClass('field-focus');
            }
            else {
                $('.player-card .player-card__selector').removeClass('active');
                $('.player-card__selector', this).addClass('active');
                switch ($('.player-card__info_value', this).html()) {
                    case 'gk':
                        $('.field__player-position').removeClass('field-focus');
                        $('.field__player-position.gk').addClass('field-focus');
                        break;
                    case 'df':
                        $('.field__player-position').removeClass('field-focus');
                        $('.field__player-position.df').addClass('field-focus');
                        break;
                    case 'mf':
                        $('.field__player-position').removeClass('field-focus');
                        $('.field__player-position.mf').addClass('field-focus');
                        break;
                    case 'at':
                        $('.field__player-position').removeClass('field-focus');
                        $('.field__player-position.at').addClass('field-focus');
                        break;
                }
            }
        }
        else {
            $('#formation-fields #' + player_card.attr("id") + '').empty();
            $('#formation-fields #' + player_card.attr("id") + '').removeClass('drafted');
            $(player_card).attr('data-draft', 'null');
            $('.player-card__selector', player_card).removeClass('draft');
            $('.player-card .player-card__selector').removeClass('active');
            $('.field__player-position').removeClass('field-focus');
            salary_cap = Math.round($('.salary-cap .leagues-listing__item_value').attr('salary-cap') * 100) / 100;
            value = Math.round($('.player-card__price', this).attr('price') * 100) / 100;
            //field_value = Math.round(field.attr('price') * 100) / 100;
            new_salary_cap = (salary_cap + value);
            new_salary_cap = new_salary_cap.toFixed(2);
            $('.salary-cap .leagues-listing__item_value').attr('salary-cap', new_salary_cap);
            $('.salary-cap .leagues-listing__item_value').html('€' + new_salary_cap + 'M');
            $('#formation-fields #' + player_card.attr("id") + '').attr('price', 0);
            $('#formation-fields #' + player_card.attr("id") + '').attr('id', 'null');
            storage = storage.filter(function (el) {return el.id !== '' + $(player_card).attr("id") + ''});
            salary_cup_storage = ({salary_cup:''+new_salary_cap+''});
        }
    });

    //add player to field
    $('body').on('click', '.field__player-position', function () {
        field = $(this);
        field_value = null;
        $('.salary-cap .leagues-listing__item_value').css('color', 'black');
        salary_cap = Math.round($('.salary-cap .leagues-listing__item_value').attr('salary-cap') * 100) / 100;
        if ($('.player-card__selector').hasClass('active')) {
            value = Math.round($('.player-card__price', player_card).attr('price') * 100) / 100;
            field_value = Math.round(field.attr('price') * 100) / 100;
            if ($('.player-card__info_value', player_card).html() == $(this).attr('data-position')) {
                if (field.hasClass('drafted')) {
                    new_salary_cap = (salary_cap - value + field_value);
                    new_salary_cap = new_salary_cap.toFixed(2);
                    if (new_salary_cap < 0) {
                        $('#nofound').modal('show');
                        $('.salary-cap .leagues-listing__item_value').css('color', 'red');
                    }
                    else {
                        storage = storage.filter(function (el) {return el.id !== '' + field.attr("id") + ''});
                        field.empty();
                        field.append(field_card);
                        $('#horiz_container #' + field.attr("id") + '').attr('data-draft', 'null');
                        $('#horiz_container #' + field.attr("id") + ' .player-card__selector').removeClass('draft');
                        player_card.attr('data-draft', 'drafted');
                        $('.player-card__selector', player_card).removeClass('active');
                        $('.player-card__selector', player_card).addClass('draft');
                        $('.salary-cap .leagues-listing__item_value').attr('salary-cap', new_salary_cap);
                        $('.salary-cap .leagues-listing__item_value').html('€' + new_salary_cap + 'M');
                        field.attr('id', $(player_card).attr('id'));
                        field.attr('price', value);
                        storage.push({id: '' + player_card.attr("id") + '', position: '' + field.attr("data-pos") + '', name: '' + $('.player-card__name',player_card).html() + '', club: '' + $('.player-card__club',player_card).html() + ''});
                        salary_cup_storage = ({salary_cup:''+new_salary_cap+''});
                    }
                }
                else {
                    new_salary_cap = (salary_cap - value);
                    new_salary_cap = new_salary_cap.toFixed(2);
                    if (new_salary_cap < 0) {
                        $('#nofound').modal('show');
                        $('.salary-cap .leagues-listing__item_value').css('color', 'red');
                    }
                    else {
                        field.addClass('drafted');
                        field.empty();
                        field.append(field_card);
                        player_card.attr('data-draft', 'drafted');
                        $('.salary-cap .leagues-listing__item_value').attr('salary-cap', new_salary_cap);
                        $('.salary-cap .leagues-listing__item_value').html('€' + new_salary_cap + 'M');
                        $('.player-card__selector', player_card).removeClass('active');
                        $('.player-card__selector', player_card).addClass('draft');
                        $('.field__player-position').removeClass('field-focus');
                        field.attr('id', $(player_card).attr('id'));
                        field.attr('price', value);
                        storage.push({id: '' + player_card.attr("id") + '', position: '' + field.attr("data-pos") + '', name: '' + $(".player-card__name",player_card).html() + '', club: '' + $('.player-card__club',player_card).html() + ''});
                        salary_cup_storage = ({salary_cup:''+new_salary_cap+''});
                    }
                }
            }
            else {
                $('#nofound').modal('hide');
                $('#wrongposition').modal('show');
            }
        }
        else {
            field_value = Math.round(field.attr('price') * 100) / 100;
            if(field.hasClass('drafted')){
                //remove players from field
                new_salary_cap = (salary_cap + field_value);
                new_salary_cap = new_salary_cap.toFixed(2);
                $('.salary-cap .leagues-listing__item_value').attr('salary-cap', new_salary_cap);
                $('.salary-cap .leagues-listing__item_value').html('€' + new_salary_cap + 'M');
                salary_cup_storage = ({salary_cup:''+new_salary_cap+''});
                storage = storage.filter(function (el) {return el.id !== '' + field.attr("id") + ''});
                field.empty();
                field.removeClass('drafted');
                $('#horiz_container #'+field.attr('id')+'').attr('data-draft', 'null');
                $('#horiz_container #'+field.attr('id')+' .player-card__selector').removeClass('draft');
                field.attr('price',0);
                field.attr('id','null');
            }
            else{
                //add player
            }
        }
    });

    // player position filter
    $('body').on('click', '.player-filter__position_buttons_item', function () {
        if ($(this).hasClass('active')) {/*do nothing*/}
        else {
            $('#horiz_container').empty();
            $('.preloader').show();
            active_btn = $(this);
            club = $('#club-filter option:selected').val();
            position = $(this).html();
            valueall = $("#ex2").bootstrapSlider('getValue');
            val1 = valueall[0] + '.' + 000;
            val2 = valueall[1] + '.' + 000;
            switch (position) {
                case 'all':
                    $('.field__player-position').removeClass('field-focus');
                    break;
                case 'gk':
                    $('.field__player-position').removeClass('field-focus');
                    $('.field__player-position.gk').addClass('field-focus');
                    break;
                case 'df':
                    $('.field__player-position').removeClass('field-focus');
                    $('.field__player-position.df').addClass('field-focus');
                    break;
                case 'mf':
                    $('.field__player-position').removeClass('field-focus');
                    $('.field__player-position.mf').addClass('field-focus');
                    break;
                case 'at':
                    $('.field__player-position').removeClass('field-focus');
                    $('.field__player-position.at').addClass('field-focus');
                    break;
            }
            data = 'club=' + club + '&position=' + position + '&min=' + val1 + '&max=' + val2+ '&type=' + league_type;
            $.ajax({
                type: "POST",
                url: "{{URL::to('draft/filter')}}",
                data: data,
                cache: true,
                success: function (result) {
                    $('.preloader').hide();
                    if (result == '') {
                        $('#horiz_container').html('<h2 style="width:50%;color:red;margin-left:15%;">No matching results found. Please modify your search criteria and try searching again.</h2>');
                        $('.player-filter__position_buttons_item').removeClass('active');
                        active_btn.addClass('active');
                    }
                    else {
                        result.forEach(function (entry) {
                            price = Math.round(entry.value * 100) / 100;
                            card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                            $('#horiz_container').append(card);
                        });
                        storage.forEach(function (index) {
                            $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                            $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                        });
                        $('.player-filter__position_buttons_item').removeClass('active');
                        active_btn.addClass('active');
                    }
                    setScroller(2000);
                    var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
                }
            }, "json");
        }
    });

    //club filter
    $('body').on('change', '#club-filter', function () {
        $('#horiz_container').empty();
        $('.preloader').show();
        position = $('.player-filter__position_buttons_item.active').html();
        club = $(this).val();
        valueall = $("#ex2").bootstrapSlider('getValue');
        val1 = valueall[0] + '.' + 000;
        val2 = valueall[1] + '.' + 000;
        data = 'club=' + club + '&position=' + position + '&min=' + val1 + '&max=' + val2+ '&type=' + league_type;
        $.ajax({
            type: "POST",
            url: "{{URL::to('draft/filter')}}",
            data: data,
            cache: true,
            success: function (result) {
                $('.preloader').hide();
                if (result == '') {
                    $('#horiz_container').html('<h2 style="width:50%;color:red;margin-left:15%;">No matching results found. Please modify your search criteria and try searching again.</h2>');
                }
                else {
                    result.forEach(function (entry) {
                        price = Math.round(entry.value * 100) / 100;
                        card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                        $('#horiz_container').append(card);
                    });
                    storage.forEach(function (index) {
                        $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                        $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                    });
                }
                setScroller(2000);
                var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
            }
        }, "json");
    });

    //price range filter
    $("#ex2").on("slideStop", function (slideEvt) {
        $('#horiz_container').empty();
        $('.preloader').show();
        valueall = slideEvt.value;
        val1 = valueall[0] + '.' + 000;
        val2 = valueall[1] + '.' + 000;
        club = $('#club-filter option:selected').val();
        position = $('.player-filter__position_buttons_item.active').html();
        data = 'club=' + club + '&position=' + position + '&min=' + val1 + '&max=' + val2+ '&type=' + league_type;
        $.ajax({
            type: "POST",
            url: "{{URL::to('draft/filter')}}",
            data: data,
            cache: true,
            success: function (result) {
                $('.preloader').hide();
                if (result == '') {
                    $('#horiz_container').html('<h2 style="width:50%;color:red;margin-left:15%;">No matching results found. Please modify your search criteria and try searching again.</h2>');
                }
                else {
                    result.forEach(function (entry) {
                        price = Math.round(entry.value * 100) / 100;
                        card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                        $('#horiz_container').append(card);
                    });
                    storage.forEach(function (index) {
                        $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                        $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                    });
                }
                setScroller(2000);
                var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
            }
        }, "json");
    });

    //search player by name
    $('body').on('click', '.ui-autocomplete li', function () {
        $('#horiz_container').empty();
        $('.preloader').show();
        name = $(this).html();
        data = 'name=' + name+ '&type=' + league_type;
        $.ajax({
            type: "POST",
            url: "{{URL::to('draft/search')}}",
            data: data,
            cache: true,
            success: function (result) {
                $('.preloader').hide();
                if (result == '') {/* do nothing*/}
                else {
                    result.forEach(function (entry) {
                        price = Math.round(entry.value * 100) / 100;
                        card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                        $('#horiz_container').append(card);
                    });
                    storage.forEach(function (index) {
                        $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                        $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                    });
                    setScroller(2000);
                    var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
                }
            }
        }, "json");
    });

    //reset filter
    $('body').one('click', '.reset-filter', function () {
        $('.preloader').show();
        $('#horiz_container').empty();
        $('.player-filter__position_buttons_item').removeClass('active');
        $('.player-filter__position_buttons_item.all').addClass('active');
        $('#club-filter option:first-child').attr('selected', 'selected');
        $("#ex2").bootstrapSlider('setValue', [0, 100]);
        $('.field__player-position').removeClass('field-focus');
        $('#autocomplete').val('');
        data = 'type=' + league_type;
        $.ajax({
            type: "POST",
            url: "{{URL::to('draft/resetfilter')}}",
            data: data,
            cache: true,
            success: function (result) {
                $('.preloader').hide();
                if (result == '') {/* do nothing*/}
                else {
                    result.forEach(function (entry) {
                        price = Math.round(entry.value * 100) / 100;
                        card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                        $('#horiz_container').append(card);
                    });
                    storage.forEach(function (index) {
                        $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                        $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                    });
                    setScroller(2000);
                    var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
                }
            }
        }, "json");
    });

    //search autocomplete
    availableTags = [];
    $('.player-card').each(function () {
        availableTags.push("" + $('.player-card__name', this).html() + "");
    });
    $("#autocomplete").keydown(function(event) {
        if (event.which == 13) {
            $('#horiz_container').empty();
            $('.preloader').show();
            name = $('#autocomplete').val();
            data = 'name=' + name+ '&type=' + league_type;
            $.ajax({
                type: "POST",
                url: "{{URL::to('draft/search')}}",
                data: data,
                cache: true,
                success: function (result) {
                    $('.preloader').hide();
                    if (result == '') {/* do nothing*/}
                    else {
                        result.forEach(function (entry) {
                            price = Math.round(entry.value * 100) / 100;
                            card = '<div class="player-card" id="' + entry.id + '"><div class="player-card__selector"></div><div class="player-card__avatar"><img src="' + entry.image + '" alt=""></div><div class="player-card__name">' + entry.name + '</div><div class="player-card__club">' + entry.club + '</div><div class="player-card__games-played"><span class="player-card__info_label">position:</span><span class="player-card__info_value">' + entry.position + '</span></div><div class="player-card__price" price="' + price + '">€' + price + 'M</div></div>';
                            $('#horiz_container').append(card);
                        });
                        storage.forEach(function (index) {
                            $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
                            $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
                        });
                        setScroller(2000);
                        var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
                    }
                }
            }, "json");
            event.preventDefault();
        }
    });
    $('#autocomplete').autocomplete({
        source: availableTags
    });

    $('body').on('click', '#autocomplete', function () {
        this.select();
    });

    //get team
    players = {{$game->pif}};
    players.forEach(function (index) {
        field_card = '<span class="field__player-position_profile-img"><img src="' + $('#horiz_container #' + index.id + ' div.player-card__avatar img').attr('src') + '" alt=""></span><span class="field__player-position_name">' + $('#horiz_container #' + index.id + ' div.player-card__name').html() + '<br><span style="color:black;font-family:roboto;">' + $('#horiz_container #' + index.id + ' .player-card__price').html() + '</span></span>';
        $('#horiz_container #' + index.id + '').attr('data-draft', 'drafted');
        $('#horiz_container #' + index.id + ' .player-card__selector').addClass('draft');
        $('.field__player-position[data-pos="' + index.position + '"]').append(field_card);
        $('.field__player-position[data-pos="' + index.position + '"]').addClass('drafted');
        $('.field__player-position[data-pos="' + index.position + '"]').attr('id', '' + index.id + '');
        $('.field__player-position[data-pos="' + index.position + '"]').attr('price', '' + $('#horiz_container #' + index.id + ' .player-card__price').attr('price') + '');
    });

    // call horizontal scroller
    setScroller(2000);
    var nicesx = $("#horiz_container_outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0.6,cursorwidth:8,usetransition:true,hwacceleration:true,autohidemode:false});
    var nicesx = $(".box-outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0,cursorwidth:0,usetransition:true,hwacceleration:true});
});


</script>
@endforeach
@stop