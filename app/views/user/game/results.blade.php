@extends ('user._layouts.results')

@section('main')


<section class="dashboard">
    <header>
        <h2>Results</h2>
        <a href="{{ URL::route('leagues') }}" class="cta-button green">Play new league</a>
        <a href="{{ URL::route('user.dashboard') }}" class="cta-button black">Back to Dashboard</a>
    </header>

    @include('bones-flash::bones.flash')

    @if ($games->count() == 0)
        <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no active players.</p>
    @endif

    @if ($games->count() > 0)
    <table class="table">
        <tr style="font-weight:bold;">
            <th>Position</th>
            <th>Player</th>
            <th>Team</th>
            <th>Score</th>
        </tr>
        <?php $i = 1; ?>
        <script>
            formation343 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-3" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-3" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-3" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';
            formation442 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-2" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-2" data-position="at" price="0" id="null" data-pos="at2"></span>';
            formation433 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-3" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-3" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-3" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';
            allPlayers = <?php echo json_encode($players); ?>;
        </script>
        @foreach($games as $game)
        <?php
        $date = $game->leagues->date;
        $time = $game->leagues->time;
        $timestamp = strtotime($date.' '. $time);
        $current_time = time()+7200;
        if($current_time > $timestamp){ ?>
            <tr>
                <td class="position{{$i}}">{{$i++}}.</td>
                <td class="nickname{{$i}}" user_id="{{$game->user_id}}">{{$game->competitors->nickname}}</td>
                <td>
                    <div class="cta">
                        <a href="" class="cta-button green js_modal" data-toggle="modal" data-target="#view_team{{ $i }}" style="font-size: 0.8rem; padding: 10px 20px; width:120px;">view team</a>
                    </div>

                    <!-- View Team dialog -->
                    <div class="modal fade" id="view_team{{ $i }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                        <div class="modal-dialog modal-lg" style="min-width: 900px !important;">
                            <div class="modal-content" style="text-align:center;">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" style="padding:0 !important;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                                    <h2 class="modal-title" id="myModalLabel" style="color:#2ba815;text-transform:uppercase;font-size:1.6rem;float:none;padding:0;">{{$game->competitors->nickname}} Team</h2>
                                </div>
                                <div class="modal-body">
                                    <div class="field-wrap" style="background:none;">
                                        <div class="field 3-4-4">
                                            <div id="formation-fields{{$i}}"></div>
                                            <img src="{{ URL::asset('assets/images/layout/football-field.jpg')}}" alt="">
                                        </div>
                                    </div>
                                    <div class="js_stats" style="display:none;text-align: left;">
                                        <div class="row">
                                            <div class="col-md-4 js_player_info" style="border-right:solid 1px #ccc;">
                                                <a href="#" class="cta-button green js_back" style="font-size: 0.8rem; padding: 10px 20px; width:120px;">back</a>
                                                <hr>
                                            </div>
                                            <div class="col-md-8 js_player_stats">
                                                <h2>Points</h2>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- End View Team dialog -->
                </td>
                <td>{{$game->score}}</td>
            </tr>
        <?php }
        else{ ?>
            <tr>
                <td class="position{{$i}}">{{$i++}}.</td>
                <td class="nickname{{$i}}" user_id="{{$game->user_id}}">{{$game->competitors->nickname}}</td>
                <td>No info</td>
                <td>{{$game->score}}</td>
            </tr>
        <?php }

        ?>
        <script>
            $(document).ready(function () {
                position = $('.position{{$i-1}}').html();
                position=parseInt(position);
                nickname = $('.nickname{{$i}}').attr('user_id');
                league_id = {{$game->league_id}};
                data = 'position='+position+'&nickname='+nickname+'&league_id='+league_id;
                $.ajax({
                    type: "POST",
                    url: "{{URL::to('user/game/position')}}",
                    data: data,
                    cache: true,
                    success: function (result) {

                    }
                }, "json");

                //set game formation
                switch('{{$game->formation}}'){
                    case '3-4-3':
                        $('#formation-fields{{$i}}').append(formation343);
                        break;
                    case '4-3-3':
                        $('#formation-fields{{$i}}').append(formation433);
                        break;
                    case '4-4-2':
                        $('#formation-fields{{$i}}').append(formation442);
                        break;
                }
                players = {{$game->pif}};
                players.forEach(function (index) {
                    field_card = '<span class="field__player-position_profile-img"><img src="'+allPlayers[index.id-1].image+'" alt=""></span><span class="field__player-position_name" style="font-size:.9rem;">' + index.name + '<br><span style="color:black;font-family:roboto;">' + index.points + '</span></span>';
                    $('#formation-fields{{$i}} .field__player-position[data-pos="' + index.position + '"]').append(field_card);
                    $('#formation-fields{{$i}} .field__player-position[data-pos="' + index.position + '"]').addClass('drafted');
                    $('#formation-fields{{$i}} .field__player-position[data-pos="' + index.position + '"]').attr('id', '' + index.id + '');
                    //$('.field__player-position[data-pos="' + index.position + '"]').attr('price', '' + $('#horiz_container #' + index.id + ' .player-card__price').attr('price') + '');
                });

                $('body').on('click','#formation-fields{{$i}} .field__player-position',function(){
                    $('.field-wrap').slideUp(200,function(){
                        $('.js_stats').slideDown(200);
                    });
                    player_id = $(this).attr('id');
                    data_stats = 'league_id='+league_id+'&player_id='+player_id;
                    $.ajax({
                        type: "POST",
                        url: "{{URL::to('user/game/stats')}}",
                        data: data_stats,
                        cache: true,
                        success: function (result) {
                            console.log(result);
                            price = Math.round(allPlayers[player_id-1].value * 100) / 100;
                            $('.js_player_info').append('<img src="'+allPlayers[player_id-1].image+'">');
                            $('.js_player_info').append('<p><strong>Name:</strong> '+allPlayers[player_id-1].name+'</p>');
                            $('.js_player_info').append('<p><strong>Team:</strong> '+allPlayers[player_id-1].club+'<p>');
                            $('.js_player_info').append('<p><strong>Value:</strong> €'+price+'M<p>');
                            switch (allPlayers[player_id-1].position){
                                case 'gk':
                                    player_position = 'Goalkeeper';
                                    break;
                                case 'df':
                                    player_position = 'Defender ';
                                    break;
                                case 'mf':
                                    player_position = 'Midfielder ';
                                    break;
                                case 'at':
                                    player_position = 'Forward ';
                                    break;
                            }
                            $('.js_player_info').append('<p><strong>Position:</strong> '+player_position+'</p>');
                            switch (allPlayers[player_id-1].position){
                                case 'gk':
                                    $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th>Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td>'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>1st received goal</th><th>Other recieved goals</th><th>Save</th><th>Claim</th><th>Penalty saved</th><th>Clean sheet</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].first_rec_goal+'</td><td>'+result[0].other_rec_goal+'</td><td>'+result[0].save+'</td><td>'+result[0].claim+'</td><td>'+result[0].penalty_saved+'</td><td>'+result[0].clean_sheet+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                                    break;
                                case 'df':
                                    $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Block</th><th>Interception</th><th>Cleareance</th><th>Clean sheet</th><th>Goal received</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].block+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].clean_sheet+'</td><td>'+result[0].rec_goal+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                                    break;
                                case 'mf':
                                    $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Touches</th><th>Interception</th><th>Cleareance</th><th>Dribbling</th><th>Goal received</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].touches+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].dribling+'</td><td>'+result[0].rec_goal+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                                    break;
                                case 'at':
                                    $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Touches</th><th>Interception</th><th>Cleareance</th><th>Dribbling</th><th>Offside</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].touches+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].dribling+'</td><td>'+result[0].offside+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                                    break;
                            }
                        }
                    }, "json");

                });
                $('body').on('click','.js_back',function(){
                    $('.js_stats').slideUp(200,function(){
                        $('.field-wrap').slideDown(200);
                    });
                    $('.js_player_info img').remove();
                    $('.js_player_info p').remove();
                    $('.js_player_stats_table').remove();

                });
                $('body').on('click','.js_modal',function(){
                    $('.field-wrap').show();
                    $('.js_stats').hide();
                    $('.js_player_info img').remove();
                    $('.js_player_info p').remove();
                    $('.js_player_stats_table').remove();
                });

            });
        </script>
        @endforeach

    </table>
    @endif

</section>

@stop
