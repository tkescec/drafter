@extends ('user._layouts.team')

@section('main')

<section class="content-wrap draft my_team dashboard">
    <header>
        <h2>My Team</h2>
        <a href="{{ URL::route('user.dashboard') }}" class="cta-button black">Back to Dashboard</a>
    </header>
    @include('bones-flash::bones.flash')
    <div class="future-games">
        <h2 class="future-games_headline">Matches</h2>
        <div class="box-outer">
            <div class="box-inner">
                @foreach ($matches as $match)
                <?php
                $date = explode('-',$match->date);
                $year = $date[0];
                $month = $date[1];
                $day = $date[2];
                $time = explode(':',$match->time);
                $h = $time[0];
                $m = $time[1];
                $date_matches = $match->date;
                $time_matches = $match->time;
                $timestamp = strtotime($date_matches.' '. $time_matches);
                $current_time = time()+7200;
                if($match->results != '-'){
                    $results = explode(':',$match->results);
                }
                else{
                    $results[0] = '';
                    $results[1] = '';
                }
                ?>
                <a href="#" class="future-games__item">
                    <span class="future-games__item_timestamp">{{$day.'/'.$month.'/'.$year}} {{$h.':'.$m}}</span>
                    <span class="future-games__item_label">{{$match->club1}} <b>{{$results[0]}}</b> - <b>{{$results[1]}}</b> {{$match->club2}}</span>
                </a>
                @endforeach
            </div>
        </div>
    </div>
    <div class="field-wrap">
        <div class="field 3-4-4">
            <div id="formation-fields"></div>
            <img src="{{ URL::asset('assets/images/layout/football-field.jpg')}}" alt="">
        </div>
    </div>
</section>
<!-- View Player Stats dialog -->
<div class="modal fade" id="view_player_stats" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" style="min-width: 900px !important;">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" style="padding:0 !important;"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel" style="color:#2ba815;text-transform:uppercase;font-size:1.6rem;float:none;padding:0;">Player Points</h2>
            </div>
            <div class="modal-body">
                <div class="preload">
                    <img src="{{ URL::asset('assets/images/preloader/328.gif')}}">
                </div>
                <div class="js_stats" style="text-align: left;">
                    <div class="row">
                        <div class="col-md-4 js_player_info" style="border-right:solid 1px #ccc;">

                        </div>
                        <div class="col-md-8 js_player_stats">

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- End View Player Stats dialog -->
<script>
    formation343 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-3" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-3" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-3" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';
    formation442 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-4" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-4" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-4" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position mf position-4-4" data-position="mf" price="0" id="null" data-pos="mf4"></span><span class="field__player-position at position-1-2" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-2" data-position="at" price="0" id="null" data-pos="at2"></span>';
    formation433 = '<span class="field__player-position gk" data-position="gk" price="0" id="null" data-pos="gk"></span><span class="field__player-position df position-1-4" data-position="df" price="0" id="null" data-pos="df1"></span><span class="field__player-position df position-2-4" data-position="df" price="0" id="null" data-pos="df2"></span><span class="field__player-position df position-3-4" data-position="df" price="0" id="null" data-pos="df3"></span><span class="field__player-position df position-4-4" data-position="df" price="0" id="null" data-pos="df4"></span><span class="field__player-position mf position-1-3" data-position="mf" price="0" id="null" data-pos="mf1"></span><span class="field__player-position mf position-2-3" data-position="mf" price="0" id="null" data-pos="mf2"></span><span class="field__player-position mf position-3-3" data-position="mf" price="0" id="null" data-pos="mf3"></span><span class="field__player-position at position-1-3" data-position="at" price="0" id="null" data-pos="at1"></span><span class="field__player-position at position-2-3" data-position="at" price="0" id="null" data-pos="at2"></span><span class="field__player-position at position-3-3" data-position="at" price="0" id="null" data-pos="at3"></span>';
    allPlayers = <?php echo json_encode($players); ?>;

    $(document).ready(function () {
        @foreach($games as $game)
        //set game formation
        switch('{{$game->formation}}'){
            case '3-4-3':
                $('#formation-fields').append(formation343);
                break;
            case '4-3-3':
                $('#formation-fields').append(formation433);
                break;
            case '4-4-2':
                $('#formation-fields').append(formation442);
                break;
        }
        players = {{$game->pif}};
        league_id = {{$game->league_id}}
        players.forEach(function (index) {
            field_card = '<span class="field__player-position_profile-img"><img src="'+allPlayers[index.id-1].image+'" alt=""></span><span class="field__player-position_name" style="font-size:.9rem;">' + index.name + '<br><span style="color:black;font-family:roboto;">' + index.points + '</span></span>';
            $('#formation-fields .field__player-position[data-pos="' + index.position + '"]').append(field_card);
            $('#formation-fields .field__player-position[data-pos="' + index.position + '"]').addClass('drafted');
            $('#formation-fields .field__player-position[data-pos="' + index.position + '"]').attr('id', '' + index.id + '');

        });
    $('body').on('click','#formation-fields .field__player-position',function(){
        $('.js_player_info img').remove();
        $('.js_player_info p').remove();
        $('.js_player_stats_table').remove();
        $('#view_player_stats').modal();
        player_id = $(this).attr('id');
        data_stats = 'league_id='+league_id+'&player_id='+player_id;
        $.ajax({
            type: "POST",
            url: "{{URL::to('user/game/stats')}}",
            data: data_stats,
            cache: true,
            success: function (result) {
                $('.preload').remove();
                price = Math.round(allPlayers[player_id-1].value * 100) / 100;
                $('.js_player_info').append('<img src="'+allPlayers[player_id-1].image+'">');
                $('.js_player_info').append('<p><strong>Name:</strong> '+allPlayers[player_id-1].name+'</p>');
                $('.js_player_info').append('<p><strong>Team:</strong> '+allPlayers[player_id-1].club+'<p>');
                $('.js_player_info').append('<p><strong>Value:</strong> €'+price+'M<p>');
                switch (allPlayers[player_id-1].position){
                    case 'gk':
                        player_position = 'Goalkeeper';
                        break;
                    case 'df':
                        player_position = 'Defender ';
                        break;
                    case 'mf':
                        player_position = 'Midfielder ';
                        break;
                    case 'at':
                        player_position = 'Forward ';
                        break;
                }
                $('.js_player_info').append('<p><strong>Position:</strong> '+player_position+'</p>');
                switch (allPlayers[player_id-1].position){
                    case 'gk':
                        $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th>Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td>'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>1st received goal</th><th>Other recieved goals</th><th>Save</th><th>Claim</th><th>Penalty saved</th><th>Clean sheet</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].first_rec_goal+'</td><td>'+result[0].other_rec_goal+'</td><td>'+result[0].save+'</td><td>'+result[0].claim+'</td><td>'+result[0].penalty_saved+'</td><td>'+result[0].clean_sheet+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                        break;
                    case 'df':
                        $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Block</th><th>Interception</th><th>Cleareance</th><th>Clean sheet</th><th>Goal received</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].block+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].clean_sheet+'</td><td>'+result[0].rec_goal+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                        break;
                    case 'mf':
                        $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Touches</th><th>Interception</th><th>Cleareance</th><th>Dribbling</th><th>Goal received</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].touches+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].dribling+'</td><td>'+result[0].rec_goal+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                        break;
                    case 'at':
                        $('.js_player_stats').append('<div class="js_player_stats_table"><table class="table"><tr><th>Minutes</th><th>Yellow card</th><th>Red card</th><th>Own goal</th><th>Asist</th><th>Accurate cross</th><th>Missed penalty</th><th colspan="2">Winning team</th></tr><tr><td>'+result[0].minutes+'</td><td>'+result[0].yellow_card+'</td><td>'+result[0].red_card+'</td><td>'+result[0].own_goal+'</td><td>'+result[0].asist+'</td><td>'+result[0].accurate_cross+'</td><td>'+result[0].missed_penalty+'</td><td colspan="2">'+result[0].winning_team+'</td></tr><tr><th>Goal scored</th><th>Pass</th><th>Shot on target</th><th>Touches</th><th>Interception</th><th>Cleareance</th><th>Dribbling</th><th>Offside</th><th>All points</th></tr><tr><td>'+result[0].goal+'</td><td>'+result[0].pass+'</td><td>'+result[0].shot_on_target+'</td><td>'+result[0].touches+'</td><td>'+result[0].interception+'</td><td>'+result[0].cleareance+'</td><td>'+result[0].dribling+'</td><td>'+result[0].offside+'</td><td>'+result[0].all_points+'</td></tr></table></div>');
                        break;
                }
            }
        }, "json");

    });
        @endforeach

        function setScroller(offset) {
            elementWidth = $('.future-games__item').width();
            numElement = $('.future-games__item').length;
            boxWidth = (numElement * elementWidth) + offset;
            $('.box-inner').css('width', boxWidth);
        }
        setScroller(2000);
        var nicesx = $(".box-outer").niceScroll({touchbehavior:true,cursorcolor:"#999",cursoropacitymax:0,cursorwidth:0,usetransition:true,hwacceleration:true});
    });
</script>
@stop
