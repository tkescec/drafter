@extends ('user._layouts.dashboard')
@section('main')
<div id="fb-root"></div>
<script src="http://connect.facebook.net/en_US/all.js"></script>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&appId=971323262892342&version=v2.3";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>


<section class="dashboard">
@include('bones-flash::bones.flash')
    <section class="dashboard-menu-wrap col">
        <header>
            <h2>Menu</h2>
        </header>
        <ul class="dashboard-menu">
            <li class="item">
                <a href="#" data-toggle="modal" data-target="#comingsoon">
                    <span class="label">Edit account</span>
                    <small class="desc"></small>
                </a>
            </li>
            <li class="item">
                <a href="#" data-toggle="modal" data-target="#comingsoon">
                    <span class="label">Saved teams</span>
                    <small class="desc"></small>
                </a>
            </li>
            <li class="item">
                <a href="#" data-toggle="modal" data-target="#comingsoon">
                    <span class="label">Fan forum</span>
                    <small class="desc"></small>
                </a>
            </li>
            <li class="item">
                <a href="http://vingd.com" target="_blank">
                    <span class="label cta">Get Vingd coins</span>
                    <small class="desc"></small>
                </a>
            </li>
        </ul>
<!--        <div class="status">-->
<!--            <span class="label">58 friends online</span>-->
<!--            <span class="label">Last active 3 hours ago on HTC mobile in Zagreb</span>-->
<!--        </div>-->
    </section>


    <section class="leagues-listing col">

        @if ($games->count() == 0)
            <header>
                <h2>Active <span class="light-cut">leagues</span></h2>
                <a href="{{ URL::route('leagues') }}" class="cta-button green right _">Play new league</a>
            </header>
            <p style="font-size:1.4rem;color:red;text-align:center;margin-top:20px">There are currently no active games.</p>
        @endif
        @if ($games->count() > 0)
            @foreach ($games as $game)
                <?php
                    $d = explode('-',$game->leagues->date);
                    $f = $d[2].'/'.$d[1].'/'.$d[0];
                    $time_array = explode(':',$game->leagues->time);
                    $time = $time_array[0].':'.$time_array[1];

                    $date = $game->leagues->date;
                    $time = $game->leagues->time;
                    $timestamp = strtotime($date.' '. $time);
                    $current_time = time()+7200;
                ?>
        @if($current_time < $timestamp)
        <header class="js_button js_active">
            <h2>Active <span class="light-cut">leagues</span></h2>
            <a href="{{ URL::route('leagues') }}" class="cta-button green right _">Play new league</a>
        </header>
        @else
        <header class="js_button js_finished">
            <h2>Finished <span class="light-cut">leagues</span></h2>
            <a href="{{ URL::route('leagues') }}" class="cta-button green right">Play new league</a>
        </header>
        @endif
        <div class="leagues-listing__item">
            <div class="leagues-listing__item_name">{{ $game->leagues->title}}</div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Position</small>
                <span class="leagues-listing__item_value">{{ $game->position }}</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">My score</small>
                <span class="leagues-listing__item_value">{{ $game->score }}</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Salary cap</small>
                <span class="leagues-listing__item_value">€{{round($game->fund_amount,2)}}M / €{{ $game->leagues->salary_cup}}M</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Entry fee</small>
                <span class="leagues-listing__item_value">{{ $game->leagues->entry_fee}}</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Players</small>
                <span class="leagues-listing__item_value">{{ $game->leagues->active_players}}/{{ $game->leagues->num_players}}</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Date</small>
                <span class="leagues-listing__item_value">{{ $f}}</span>
            </div>
            <div class="leagues-listing__item_block">
                <small class="leagues-listing__item_label">Time</small>
                <span class="leagues-listing__item_value">{{ $time}}</span>
            </div>
            <div class="leagues-listing__item_block cta">

                    @if($current_time > $timestamp)
                        <a href="{{ URL::route('user.game.results', $game->leagues->id) }}" class="cta-button green">View results</a>
                        <a href="{{ URL::route('user.game.team', $game->id) }}" class="cta-button black">My team</a>

                    @else
                        <a href="{{ URL::route('user.game', $game->id) }}" class="cta-button green">Edit team</a>
                        <a href="{{ URL::route('user.game.results', $game->leagues->id) }}" class="cta-button black">Teams</a>
                        <a href="https://www.facebook.com/dialog/share?app_id=971323262892342&display=touch&href=http://playdrafter.com/draft/{{$game->leagues->id}}/{{$game->leagues->type}}&redirect_uri=http://playdrafter.com/user/dashboard" class="cta-button black">Share on Facebook</a>
                        <a href="" data-toggle="modal" data-target="#challenge_friend{{ $game->leagues->id }}" id="challenge" class="cta-button black">Challenge friend</a>
                    @endif
            </div>
            <script>
                $(function () {
                    $('[data-toggle="tooltip"]').tooltip();
                });
            </script>
        </div>
                <!-- Challenge friend dialog -->
        <div class="modal fade" id="challenge_friend{{ $game->leagues->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="text-align:center;">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h2 class="modal-title" id="myModalLabel" style="color:#51c44d;text-transform:uppercase;font-size:1.6rem;">challenge friend</h2>
                    </div>
                    <div class="modal-body">
                        <button type="button" class="btn btn-default email-invite" ><i class="fa fa-envelope fa-lg"></i> Invite by email</button>

                    </div>
                    <div class="modal-footer">
                        <div class="email-invite{{$game->leagues->id}}" style="margin:0 auto;width:400px;display:none;">
                            {{ Form::open(array('id'=>'invite-form'.$game->leagues->id.'', 'class'=>'standalone-form','style' => 'margin:0 auto;')) }}
                            <fieldset>
                                @if(! Sentry::check())
                                <div class="standalone-form__form-field">
                                    {{ Form::text('nickname', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter your name')) }}
                                </div>
                                @endif
                                <div class="standalone-form__form-field">
                                    {{ Form::text('friend-name', '', array('class' => 'form-field__input_text', 'placeholder' => 'Enter friend name')) }}
                                </div>
                                <div class="standalone-form__form-field">
                                    {{ Form::email('email', '', array('class' => 'form-field__input_text email', 'placeholder' => 'Enter friend email')) }}
                                </div>
                                <div class="standalone-form__form-field">
                                    <a href="#" class="cta-button green" id="send-invitation{{ $game->leagues->id }}">Send Invitation</a>
                                </div>
                            </fieldset>
                            {{ Form::close() }}
                        </div>
                        <div class="facebook-invite">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END: challenge friend dialog -->

        <script>
            $(document).ready(function () {
                //user info
                user_info = '{{$user}}';
                league_id{{ $game->leagues->id }} = '{{ $game->leagues->id }}';
                league_name{{ $game->leagues->id }} = '{{ $game->leagues->title }}';
                $('#challenge_friend{{ $game->leagues->id }} .modal-body').append('<a href="http://www.facebook.com/dialog/send?app_id=971323262892342&link=http://playdrafter.com/draft/' + league_id{{ $game->leagues->id }} + '/{{ $game->leagues->type }} &display=page&redirect_uri=http://playdrafter.com/user/dashboard" type="button" class="btn btn-primary facebook-invite"><i class="fa fa-facebook-square fa-lg"></i> Invite by facebook</a>');
                email_open = false;
                facebook_open = false;
                //invite friends
                $('body').on('click','.challenge',function(){
                    $('div.email-invite{{$game->leagues->id}}').hide();
                    $('div.facebook-invite').hide();
                    email_open = false;
                    facebook_open = false;
                });
                $('body').on('click','button.email-invite',function(){
                    if(facebook_open === true){
                        $('div.facebook-invite').slideUp(200,function(){
                            $('div.email-invite{{$game->leagues->id}}').slideDown(200);
                            email_open = true;
                        });
                    }
                    else{
                        $('div.email-invite{{$game->leagues->id}}').slideDown(200);
                        email_open = true;
                    }
                });
                $('body').on('click','button.facebook-invite',function(){
                    if(email_open === true){
                        $('div.email-invite{{$game->leagues->id}}').slideUp(200,function(){
                            $('div.facebook-invite').slideDown(200);
                            facebook_open = true;
                        });
                    }
                    else{
                        $('div.facebook-invite').slideDown(200);
                        facebook_open = true;
                    }
                });
                $('body').on('click', '#send-invitation{{ $game->leagues->id }}', function(){
                    if($('.email-invite{{$game->leagues->id}} input[name=nickname]').length == 0){
                        email = $('.email-invite{{$game->leagues->id}} input[name=email]').val();
                        friend_name = $('.email-invite{{$game->leagues->id}} input[name=friend-name]').val();
                        console.log(email);
                        $('#invite-form{{ $game->leagues->id }}').prepend('<div class="preload" style="padding:10px 0;margin:0;text-align:center;"><img src="{{ URL::asset("assets/images/preloader/328.gif")}}"></div>');
                        if(email == '' || friend_name == ''){
                            $('.preload').remove();
                            $('.alert-danger').remove();
                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter email address and friend name.</div>');
                            $('.alert-danger').delay(2000).slideUp();
                        }
                        else{
                            if(isValidEmailAddress(email)){
                                user = JSON.parse(user_info);
                                user = $.makeArray( user );
                                $.map(user, function(key){
                                    user_name = (key.nickname);
                                });
                                data = 'league_id={{ $game->leagues->id }}&email='+email+'&user_name='+user_name+'&friend_name='+friend_name+'&league_name='+league_name{{ $game->leagues->id }};
                                $.ajax({
                                    type: "POST",
                                    url: "{{URL::to('draft/invite')}}",
                                    data: data,
                                    cache: true,
                                    success: function (result) {
                                        $('.preload').remove();
                                        if (result == 'sent') {
                                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-success" role="alert" style="text-align:center">Your email has been sent!</div>');
                                            $('.alert-success').delay(2000).slideUp();
                                        }
                                        else {
                                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Something went wrong. Please try again.</div>');
                                            $('.alert-danger').delay(2000).slideUp();
                                        }
                                    }
                                }, "json");
                            }
                            else{
                                $('.preload').remove();
                                $('.alert-danger').remove();
                                $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter correct email address.</div>');
                                $('.alert-danger').delay(2000).slideUp();
                            }
                        }
                    }
                    else{
                        email = $('.email-invite{{$game->leagues->id}} input[name=email]').val();
                        nickname = $('#invite-form{{ $game->leagues->id }} input[name=nickname]').val();
                        friend_name = $('#invite-form{{ $game->leagues->id }} input[name=friend-name]').val();
                        $('#invite-form{{ $game->leagues->id }}').prepend('<div class="preload" style="padding:10px 0;margin:0;text-align:center;"><img src="{{ URL::asset("assets/images/preloader/328.gif")}}"></div>');
                        if(email == '' || nickname == '' || friend_name == ''){
                            $('.preload').remove();
                            $('.alert-danger').remove();
                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter email address, your name and friend name.</div>');
                            $('.alert-danger').delay(2000).slideUp();
                        }
                        else{
                            if(isValidEmailAddress(email)){
                                data = 'league_id={{ $game->leagues->id }}&email='+email+'&user_name='+nickname+'&friend_name='+friend_name+'&league_name='+league_name{{ $game->leagues->id }};
                                $.ajax({
                                    type: "POST",
                                    url: "{{URL::to('draft/invite')}}",
                                    data: data,
                                    cache: true,
                                    success: function (result) {
                                        $('.preload').remove();
                                        if (result == 'sent') {
                                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-success" role="alert" style="text-align:center">Your email has been sent!</div>');
                                            $('.alert-success').delay(2000).slideUp();
                                        }
                                        else {
                                            $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Something went wrong. Please try again.</div>');
                                            $('.alert-danger').delay(2000).slideUp();
                                        }
                                    }
                                }, "json");
                            }
                            else{
                                $('.preload').remove();
                                $('.alert-danger').remove();
                                $('#invite-form{{ $game->leagues->id }}').prepend('<div class="alert alert-danger" role="alert" style="text-align:center">Please enter correct email address.</div>');
                                $('.alert-danger').delay(2000).slideUp();
                            }
                        }
                    }
                });
                function isValidEmailAddress(emailAddress) {
                    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
                    return pattern.test(emailAddress);
                }
            });

        </script>

        @endforeach
    @endif

    </section>

    <section class="news-feed-wrap col">
        <header>
            <h2>Weekly <span class="light-cut">news</span></h2>
        </header>
        <div class="news-feed">
            @foreach ($news as $data)
            <div class="item">
                <?php $time = time();    ?>
                <span class="timestamp">{{$data->updated_at->diffForHumans()}}</span>
                <h3>{{$data->title}}</h3>
                <p>{{$data->content}}</p>
            </div>
            @endforeach
            {{ $news->links() }}
            <script>
                $(document).ready(function(){
                    var ias = jQuery.ias({
                        container:  '.news-feed',
                        item:       '.item',
                        pagination: '.pagination',
                        next:       'a[rel="next"]'
                    });
                    ias.extension(new IASSpinnerExtension());
                    //ias.extension(new IASNoneLeftExtension({text: "No more news!"}));
                });
            </script>
        </div>
    </section>

</section> <!-- END: dashboard -->

<div class="modal fade" id="comingsoon" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"aria-hidden="true">
    <div class="modal-dialog modal-sm modal-dialog-fill">
        <div class="modal-content" style="text-align:center;padding:20px;">
            Coming soon!
        </div>
    </div>
</div>
<script>
    $(function () {
        $('.js_finished').first().css('display','block');
        $('.js_active').first().css('display','block');
        $('.js_button a').first().css('display','block');
    });
</script>
@stop
