<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:title" content="{{$game->leagues->title}}">
    <meta property="og:description" content="Will you accept the challenge or will you run away?">
    <meta property="og:type" content="game" />
    <meta property="og:url" content="http://playdrafter.com/draft/{{ $game->leagues->id }}" />
    <meta property="og:image" content="{{ URL::asset('assets/images/layout/logo-black-big1.png')}}" />
    <title>Draft | Drafter</title>
    @include('_partials.assets')
</head>

<body class="leagues">
@include('_partials.header')
@yield('main')
@include('_partials.footer')
</body>
</html>