<!DOCTYPE html>
<html>
<head><meta charset="utf-8"></head>
<body>

<h1 style="font-family: Arial, sans-serif; font-size: 24px; font-weight: bold; color: #222;">You've been challenged</h1>
<p style="font-family: Arial, sans-serif; font-size: 15px; color: #222;">{{$nickname}} challenged you to join the <b>{{$league_name}}</b> league!</p>
<p style="font-family: Arial, sans-serif; font-size: 15px; color: #222;">{{$friend_name}} will you accept the challenge, or are you going to run away?<br>Show your football knowledge and SMASH the competition!</p>

<p style="margin-top: 20px;">
<a href="{{URL::route('draft', $league_id)}}" style="background-color: #51c44d; color: #fff; padding: 8px 14px; text-decoration: none; border-radius: 2px; font-weight: bold; font-family: Arial, sans-serif; text-transform: uppercase; font-size: 12px;">Play now</a>
</p>

<br>----
<p>
<a href="https://www.facebook.com/PlayDrafter" style="font-family: Arial, sans-serif; color: #51c44d; font-size: 12px;">Find us on Facebook</a><br>
<a href="mailto:info@playdrafter.com?subject=Report" style="font-family: Arial, sans-serif; color: #51c44d; font-size: 12px;">If you received this email and think is's a mistake please let us know. Thank you!</a>
</p>

</body>
</html>
