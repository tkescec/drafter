<!DOCTYPE html>
<html>
<head><meta charset="utf-8"></head>
<body>

<h1 style="font-family: Arial, sans-serif; font-size: 24px; font-weight: bold; color: #222;">Hello and welcome to the Drafter Fantasy football game</h1>
<p style="font-family: Arial; font-size: 15px; color: #222;">You are succesfully subscribed for our newsletter.<br>
Now create your favorite team and challenge some friends.</p>

<p style="margin-top: 20px;">
<a href="{{ URL::route('leagues') }}" style="background-color: #51c44d; color: #fff; padding: 8px 14px; text-decoration: none; border-radius: 2px; font-weight: bold; font-family: Arial, sans-serif; text-transform: uppercase; font-size: 12px;">Play now</a>
</p>


<br>----
<p>
<a href="https://www.facebook.com/PlayDrafter" style="font-family: Arial, sans-serif; color: #51c44d; font-size: 12px;">Find us on Facebook</a><br>
<a href="{{URL::route('newsletter.unsubscribe', array('email'=>$email))}}" style="font-family: Arial, sans-serif; color: #51c44d; font-size: 12px;">Unsubscribe!</a>
<a href="mailto:info@playdrafter.com?subject=Report" style="font-family: Arial, sans-serif; color: #51c44d; font-size: 12px;">If you received this email and think is's a mistake please let us know. Thank you!</a>
</p>

</body>
</html>
