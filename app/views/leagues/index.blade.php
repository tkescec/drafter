@extends ('_layouts.leagues')

@section('main')
<section class="leagues-listing">
    @if ($leagues->count() == 0)
    <p style="font-size:1.4rem;color:red;text-align:center;">There are currently no active leagues.</p>
    @endif
    @if ($leagues->count() > 0)
        @foreach ($leagues as $league)
            <?php
            $date_matches = $league->date;
            $time_matches = $league->time;
            $timestamp = strtotime($date_matches.' '. $time_matches);
            $current_time = time()+7200;
            ?>
            <div class="dashboard">
                @if($current_time < $timestamp)
                <header class="js_button js_active">
                    <h2>Active <span class="light-cut">leagues</span></h2>
                </header>
                @else
                <header class="js_button js_finished">
                    <h2>Finished <span class="light-cut">leagues</span></h2>
                </header>
                @endif
            </div>
            <div href="#" class="leagues-listing__item">
                <div class="leagues-listing__item_name">{{ $league->title }}</div>
                <div class="leagues-listing__item_block salary-cup">
                    <small class="leagues-listing__item_label">salary cap</small>
                    <span class="leagues-listing__item_value">€{{ $league->salary_cup }}.000.000</span>
                </div>
                <div class="leagues-listing__item_block entry-fee">
                    <small class="leagues-listing__item_label">entry-fee</small>
                    <span class="leagues-listing__item_value">{{ $league->entry_fee }}</span>
                </div>
                <div class="leagues-listing__item_block players">
                    <small class="leagues-listing__item_label">no od players</small>
                    <span class="leagues-listing__item_value">{{ $league->active_players }}/{{ $league->num_players }}</span>
                </div>
                <div class="leagues-listing__item_block time">
                    <small class="leagues-listing__item_label">time left</small>
                    <span class="leagues-listing__item_value" id="field{{ $league->id }}">00 days 00:00:00</span>
                </div>
                @if($current_time < $timestamp)
                    <div class="leagues-listing__item_block cta"><a href="{{URL::route('draft', array('id'=>$league->id,'type'=>$league->type))}}" class="cta-button green play" id="button{{ $league->id }}">play</a></div>
                    <div class="leagues-listing__item_block cta"><a href="#" class="cta-button black info" id="{{ $league->id }}">info</a></div>
                    <div class="leagues-listing__item_block cta"><a href="#" class="cta-button black js_act_play" league_id="{{ $league->id }}">active players</a></div>
                @else
                    <div class="leagues-listing__item_block cta"><a href="{{URL::route('draft', array('id'=>$league->id,'type'=>$league->type))}}" class="cta-button green play disabled" disabled id="button{{ $league->id }}">play</a></div>
                    <div class="leagues-listing__item_block cta"><a href="#" class="cta-button black info" id="{{ $league->id }}">info</a></div>
                @endif
            </div>
        @endforeach
    @endif
</section>
<!------------------ Info dialog ---------------->
<div class="modal fade" id="league_info" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">Info</h2>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <h2>Matches</h2>
                    <table class="table table-hover matches">
                        <tr style="font-weight: bold;background:#eee;">
                            <th style="text-align: center;">Home</th>
                            <th style="text-align: center;">Results</th>
                            <th style="text-align: center;">Guest</th>
                            <th style="text-align: center;">Date</th>
                            <th style="text-align: center;">Time</th>
                        </tr>
                    </table>
                </div>
                <div class="table-responsive">
                    <h2>Prizes</h2>
                    <table class="table table-hover prizes">

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------ End Info dialog---------------->
<!------------------ Active players dialog ---------------->
<div class="modal fade" id="league_act_play" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="text-align:center;">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h2 class="modal-title" id="myModalLabel" style="color:#a80b27;text-transform:uppercase;font-size:1.6rem;">Active players</h2>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-hover players">

                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!------------------ End Active players dialog---------------->
<script>
    function counter(field, time, button) {

        // access to html element
        var counter_field = document.getElementById(field);
        var button_field = document.getElementById(button);

        // destination date/time
        var date_stop = time; //new Date(2013,6,7,0,0,0,0);

        // call counting function every second (1000 ms)
        var counting_loop = setInterval(counting, 1000);

        // counting function
        function counting()
        {
            // current date/time
            var date_now = new Date();

            if( date_stop == null ) {
                var text = "- you forget date/time -";
            } else {

                // convert miliseconds to seconds
                var diff = Math.round((date_stop-date_now)/1000);

                // still in future
                if( (0 < diff) )
                {
                    // convert diff to days, hours, minutes, seconds

                    // seconds
                    var seconds = diff % 60;
                    diff = (diff-seconds) / 60;
                    if( seconds < 10 ) seconds = "0" + seconds;

                    // minutes
                    var minutes = diff % 60;
                    diff = (diff-minutes) / 60;
                    if( minutes < 10 ) minutes = "0" + minutes;

                    // hours
                    var hours = diff % 24;
                    diff = (diff-hours) / 24;
                    if( hours < 10 ) hours = "0" + hours;

                    // days
                    var days = diff;
                    if( days < 10 ) days = "0" + days;

                    // convert to text
                    text = days+" days " +hours+":"+minutes+":"+seconds;
                } else {
                    // removing counter
                    button_field.classList.add("disabled");
                    button_field.removeAttribute("href");
                    counter_field.innerHTML = "THE END";

                    unsetInterval(counting_loop);

                }
            }
            // put text into html
            counter_field.innerHTML = text;
        }
    }

    window.onload = function()
    {
        @foreach ($leagues as $league)
        <?php
            $date = explode('-',$league->date);
            $year = $date[0];
            $month = $date[1] - 1;
            $day = $date[2];
            $time = explode(':',$league->time);
            $hours = $time[0];
            $minutes = $time[1];
            $seconds = $time[2];
            ?>
            counter("field{{ $league->id }}", new Date({{$year}},{{$month}},{{$day}},{{$hours}},{{$minutes}},{{$seconds}},0), "button{{ $league->id }}");
        @endforeach
    }

    $(document).ready(function(){

        $('.cta-button.play').on('click',function(){
            localStorage.clear();
        });
        @foreach ($leagues as $league)
            if({{ $league->active_players }} >= {{ $league->num_players }}){
                $("#button{{ $league->id }}").removeAttr('href');
                $("#button{{ $league->id }}").addClass('disabled');
            }
        @endforeach
        $('.cta-button.info').on('click',function(){
            $('#league_info .modal-body table tr td').remove();
            league_id = $(this).attr('id');
            data = 'league_id=' + league_id;
            $.ajax({
                type: "POST",
                url: "{{URL::to('leagues/matches')}}",
                data: data,
                cache: true,
                success: function (result) {
                    if (result == ''){}
                    else{
                        result.forEach(function (entry) {
                            dateArray = entry.date.split('-');
                            date = dateArray[2]+'/'+dateArray[1]+'/'+dateArray[0];
                            timeArray = entry.time.split(':');
                            time = timeArray[0]+':'+timeArray[1];
                            card = '<tr style="font-weight: normal;"><td>'+entry.club1+'</td><td>'+entry.results+'</td><td>'+entry.club2+'</td><td>'+date+'</td><td>'+time+'</td></tr>';
                            $('#league_info .modal-body table.matches').append(card);
                        });
                    }
                }
            }, "json");

            $.ajax({
                type: "POST",
                url: "{{URL::to('leagues/prizes')}}",
                data: data,
                cache: true,
                success: function (result) {
                    if (result == ''){
                        card = '<tr style="font-weight: normal;"><td>No prizes!</td></tr>';
                        $('#league_info .modal-body table.prizes').append(card);
                        $('#league_info').modal();
                    }
                    else{
                        result.forEach(function (entry) {
                            card = '<tr style="font-weight: normal;"><td>'+entry.position+'. place</td><td>'+entry.prize+' kn</td></tr>';
                            $('#league_info .modal-body table.prizes').append(card);
                        });
                        $('#league_info').modal();
                    }
                }
            }, "json");
        });

        $('.cta-button.js_act_play').on('click',function(){
            $('#league_act_play .modal-body table tr td').remove();
            league_id = $(this).attr('league_id');
            data = 'league_id=' + league_id;
            $.ajax({
                type: "POST",
                url: "{{URL::to('leagues/players')}}",
                data: data,
                cache: true,
                success: function (result) {
                    if (result == ''){}
                    else{
                        i=1;
                        result.forEach(function (entry) {
                            card = '<tr style="font-weight: normal;"><td>'+ i++ +'.</td><td>'+entry.competitors.nickname+'</td></tr>';
                            $('#league_act_play .modal-body table.players').append(card);
                        });
                        $('#league_act_play').modal();
                    }
                }
            }, "json");
        });
        $('.js_finished').first().css('display','block');
        $('.js_active').first().css('display','block');
        $('.js_button a').first().css('display','block');
    });
</script>
@stop