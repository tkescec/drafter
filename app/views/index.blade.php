<!DOCTYPE html>
<!--[if IE 7]>
<html class="ie ie7" lang="en">
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" lang="en">
<![endif]-->
<!--[if !(IE 7) | !(IE 8) ]><!-->
<html lang="en">
<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Drafter - Football (R)Evolution</title>

    <!-- for Google -->
    <meta name="description" content="Pick any 11 players from Premiership. Follow their performance. Enter competition. Win prizes!" >
    <meta name="keywords" content="fantasy football, fantasy premier league, premier league fantasy football, daily fantasy sports, online football game, daily fantasy football, play fantasy football, fantasy football game, daily fantasy soccer, play premier league fantasy football, daily fantasy premier league, daily premier league fantasy football" >

    <!-- for Facebook -->
    <meta property="og:title" content="Drafter - Football (R)Evolution" />
    <meta property="og:type" content="Game" />
    <meta property="og:image" content="http://playdrafter.com/assets/images/layout/fb_index.png" />
    <meta property="og:url" content="http://playdrafter.com" />
    <meta property="og:description" content="Pick any 11 players from Premiership. Follow their performance. Enter competition. Win prizes!" />

    <!-- for Twitter -->
    <meta name="twitter:card" content="summary" />
    <meta name="twitter:title" content="Drafter - Football (R)Evolution" />
    <meta name="twitter:description" content="Pick any 11 players from Premiership. Follow their performance. Enter competition. Win prizes!" />
    <meta name="twitter:image" content="http://playdrafter.com/assets/images/layout/fb_index.png" />

    @include('_partials.assets')

</head>
<body>
<script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
        (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
        m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga');
    ga('create', 'UA-61443415-1', 'auto');
    ga('send', 'pageview');
</script>
<div class="index-hero-area">

    <header class="main-header">
        <a href="{{ URL::route('/') }}" class="main-header__logo"><img src="assets/images/layout/logo-white.png" alt="Drafter"></a>
        <nav class="main-header__navigation">
            <ul class="menu">
                @if(! Sentry::check())
                <li><a href="{{ URL::route('auth.register') }}" class="main-header__navigation_item">
                        sign up
                    </a></li>
                <li><a href="{{ URL::route('auth.login') }}" class="main-header__navigation_item">
                        log in
                    </a></li>
                @else
                <li><a href="{{ URL::route('user.dashboard') }}" class="main-header__navigation_item">Dashboard</a></li>
                <li><a href="{{ URL::route('leagues') }}" class="main-header__navigation_item leagues">Play Leagues</a></li>
                <li><a href="#" class="main-header__navigation_item">
                        {{ $user = Sentry::getUser()->nickname; }}
                    </a>
                    <ul class="submenu">
                        <li>
                            <a href="{{ URL::route('auth.logout') }}" class="main-header__navigation_item">Logout</a>
                        </li>
                    </ul>
                </li>
                @endif
            </ul>
        </nav>
    </header>

    <div class="index-hero-area__copy">
        <h1>Football (r)evolution</h1>
        <h2>On a mission to take fantasy football to the next level</h2>
    </div>
    <div class="index-hero-area__cta play-now">
        <a href="{{ URL::route('leagues') }}" class="index-hero-area__cta_button cta-button green">play now</a>
    </div>
</div>
<div class="index-how-to-play">
    <h2>How to play</h2>
    <div class="index-how-to-play_item">
        <img src="assets/images/layout/index-icon-1.png" alt="">
        <h2>Enter free or pro league</h2>
        <p>Select by entry fee, salary cap ammount and number of challengers</p>
    </div>
    <div class="index-how-to-play_item">
        <img src="assets/images/layout/index-icon-2.png" alt="">
        <h2>Build your winning team</h2>
        <p>Select any 11 players from Premiership</p>
    </div>
    <div class="index-how-to-play_item">
        <img src="assets/images/layout/index-icon-3.png" alt="">
        <h2>Show football knowledge</h2>
        <p>Enter tournaments and win prizes</p>
    </div>
</div>
<div class="index-game-rules">
    <div class="index-game-rules__inner">
        <div class="index-game-rules__image">
            <img src="assets/images/layout/index-mobile.png" alt="">
        </div>
        <div class="index-game-rules__copy">
            <h2>Game rules are simple and designed for max fun</h2>
            <ul>
                <li>Enjoy in football matches on entirely new level <span>+</span></li>
                <li>Follow your favorite players real action performance on the field <span>+</span></li>
                <li>Every game becomes important <span>+</span></li>
                <li>Be engagged and use your football knowledge <span>+</span></li>
            </ul>
            <p><a href="{{URL::route('rules')}}">Read game rules</a></p>
        </div>
        <div class="clear"></div>
    </div>
</div>
<div class="index-hero-area__cta newsletter">
    <h3 class="index-hero-area__cta_headline">Get news about Drafter</h3>
    <div class="err_msg"></div>
    <fieldset class="index-hero-area__cta_fieldset">
        <input type="text" placeholder="Your email address" id="email">
        <input type="submit" value="sweet!" class="index-hero-area__cta_button cta-button transparent" id="subscribe">
        <div class="submit-preloader"><img src="assets/images/preloader/714.gif" alt=""></div>
    </fieldset>
</div>
@include('_partials.footer')
<script>
    $(document).ready(function(){
        $('body').on('click','#subscribe',function(){
            email = $('#email').val();
            data = 'email='+email;
            $('.err_msg').empty();
            $('#subscribe').hide();
            $('.alert-danger').hide();
            $('.alert-success').hide();
            $('.submit-preloader').show();
            $.ajax({
                type: "POST",
                url: "{{URL::to('newsletter/subscribe')}}",
                data: data,
                cache: false,
                success: function (result) {

                    if (result == '') {
                        $('.err_msg').prepend('<div class="alert alert-danger" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">Something went wrong! Please try again!</div>');
                        $('.alert-danger').slideDown();
                        $('.alert-danger').delay(2000).slideUp();
                        $('.submit-preloader').hide();
                        $('#subscribe').show();
                    }
                    else if(result == 'exist'){
                        $('.err_msg').prepend('<div class="alert alert-danger" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">This email is already on the newsletter list!</div>');
                        $('.alert-danger').slideDown();
                        $('.alert-danger').delay(2000).slideUp();
                        $('.submit-preloader').hide();
                        $('#subscribe').show();
                    }
                    else if(result == 'saved' || result == 'actived'){
                        $('.err_msg').prepend('<div class="alert alert-success" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">Thank you for subscribing!</div>');
                        $('.alert-success').slideDown();
                        $('.alert-success').delay(2000).slideUp();
                        $('.submit-preloader').hide();
                        $('#subscribe').show();
                    }
                    else {
                        $('.err_msg').prepend('<div class="alert alert-danger" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">'+result.email+'</div>');
                        $('.alert-danger').slideDown();
                        $('.alert-danger').delay(2000).slideUp();
                        $('.submit-preloader').hide();
                        $('#subscribe').show();
                    }
                }
            }, "json");
        });
    });
</script>
</body>
</html>
