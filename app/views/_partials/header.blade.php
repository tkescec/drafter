<header class="main-header">
    <a href="{{ URL::route('/') }}" class="main-header__logo"><img src="{{ URL::asset('assets/images/layout/logo-black.png')}}" alt="Drafter"></a>
    <nav class="main-header__navigation">
        <ul class="menu">
            <li><a href="{{ URL::route('user.dashboard') }}" class="main-header__navigation_item">Dashboard</a></li>
            <li><a href="{{ URL::route('leagues') }}" class="main-header__navigation_item leagues">Play Leagues</a></li>
            @if(! Sentry::check())
            <li><a href="{{ URL::route('auth.login') }}" class="main-header__navigation_item">
                Login
            </a></li>
            @else
            <li><a href="#" class="main-header__navigation_item">
                {{ $user = Sentry::getUser()->nickname; }}
            </a>
                <ul class="submenu">
                    <li>
                        <a href="{{ URL::route('auth.logout') }}" class="main-header__navigation_item">Logout</a>
                    </li>
                </ul>
            </li>
            @endif
        </ul>
    </nav>
</header>