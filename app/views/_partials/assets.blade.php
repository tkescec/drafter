 <!--############ GOOGLE FONTS #################-->
<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:100,300,400,700' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Roboto:100,300,400,500,700,900' rel='stylesheet'>
<link href='http://fonts.googleapis.com/css?family=Oswald:300,400,700&subset=latin,latin-ext' rel='stylesheet'>

<!--############ FONT AWESOME #################-->
<link href='http://maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' rel='stylesheet'>

<!--############ MAIN CSS FILE #################-->
<link href="{{ URL::asset('assets/css/style.css') }}" rel="stylesheet">
<link href="{{ URL::asset('assets/css/my-style.css') }}" rel="stylesheet">
<!--############ HORIZONTAL SCROLL CSS FILE #################-->
<link href="{{ URL::asset('assets/css/jquery.horizontal.scroll.css') }}" rel="stylesheet">
<!--############ BOOTSTRAP SLIDER CSS #################-->
<link href="{{ URL::asset('assets/css/bootstrap-slider.css') }}" rel="stylesheet">



<!--[if lt IE 9]>
<script src="http://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="http://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!--############ JQUERY LIBRARY #################-->
<script src="http://code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
<script src="{{ URL::asset('assets/js/json2.js') }}"></script>
<!--############ STICKY HEADER #################-->
<!-- <script src="{{ URL::asset('assets/js/StickyHeader.js') }}"></script> -->
<!--############ HORIZONTAL SCROLL #################-->
<script src="{{ URL::asset('assets/js/jquery.horizontal.scroll.js') }}"></script>
<!--############ BOOTSTRAP #################-->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.1/js/bootstrap.min.js"></script>
<!--############ BOOTSTRAP SLIDER #################-->
<script src="{{ URL::asset('assets/js/bootstrap-slider.js') }}"></script>
 <script src="{{ URL::asset('assets/js/jquery-ias.min.js') }}"></script>


