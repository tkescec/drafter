<footer class="main-footer">
    <ul class="main-footer__list">
        <li class="main-footer__list_item">© Drafter 2015</li>
        <li class="main-footer__list_item"><a href="https://www.facebook.com/PlayDrafter" target="_blank">Find us on Facebook</a></li>
        <li class="main-footer__list_item"><a href="mailto:info@playdrafter.com?subject=Feedback">Send feedback, we'd love to hear from you</a></li>
    </ul>
</footer>
