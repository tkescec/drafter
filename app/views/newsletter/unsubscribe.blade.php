@extends ('_layouts.unsubscribe')

@section('main')

<div class="standalone-form center">
    <div class="err_msg"></div>
    <p>You are about to unsubscribe from mailing list</p>
    <a href="#" id="unsubscribe" class="cta-button green small">Unsubscribe!</a>
</div>

<script>
    $(document).ready(function(){
        email = '{{$email}}';
        $('body').on('click','#unsubscribe',function(){
            data = 'email='+email;
            $.ajax({
                type: "POST",
                url: "{{URL::to('newsletter/unsubscribe')}}",
                data: data,
                cache: false,
                success: function (result) {

                    if(result == 'saved'){
                        $('.err_msg').prepend('<div class="alert alert-success" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">You have successfully unsubscribe from newsletter list</div>');
                        $('.alert-success').slideDown();
                        $('.alert-success').delay(2000).slideUp();
                    }
                    else {
                        $('.err_msg').prepend('<div class="alert alert-danger" role="alert" style="text-align:center;width:500px;margin:10px auto;display:none;">Something went wrong! Please try again!</div>');
                        $('.alert-danger').slideDown();
                        $('.alert-danger').delay(2000).slideUp();
                    }
                }
            }, "json");
        });
    });
</script>
@stop