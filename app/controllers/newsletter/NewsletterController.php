<?php namespace App\Controllers\Newsletter;

use App\Services\Validators\SubscriberValidator;
use App\Models\Subscribers;

use BaseController, View, Ajax, Input, Request,Mail,Redirect;

class NewsletterController extends \BaseController {

    public function __construct()
    {

    }

    public function subscribe()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            $email = $data['email'];
            $time = time();
            $date = date('Y-m-d H:i:s',$time);
            $validator = new SubscriberValidator;
            if($validator->passes())
            {
                $subscriber = Subscribers::where('email',$email)->get();
                if(count($subscriber) == 0){
                    $subscribe = new Subscribers();
                    $subscribe->email = $email;
                    $subscribe->active = 1;
                    $subscribe->subscribed = $date;
                    $subscribe->save();
                    Mail::send('users.mails.subscribe', array('email'=>$email), function($message){
                        $message->to(Input::get('email'))->subject('Subscription to the newsletter  | Drafter Fantasy Football League!');
                    });
                    return 'saved';
                }
                else{
                    foreach ($subscriber as $element){
                        $id = $element->id;
                        $active = $element->active;
                    }
                    if(isset($active) && $active == 0){
                        $actived = Subscribers::find($id);
                        $actived->active = 1;
                        $actived->unsubscribed = '0000-00-00 00:00:00';
                        $actived->save();
                        Mail::send('users.mails.subscribe', array('email'=>$email), function($message){
                            $message->to(Input::get('email'))->subject('Subscription to the newsletter  | Drafter Fantasy Football League!');
                        });
                        return 'actived';
                    }
                    if(isset($active) && $active == 1){
                        return 'exist';
                    }
                }
            }
            return $validator->errors;
        }
        return '';
    }

    public function unsubscribePage($email){
        return \View::make('newsletter.unsubscribe',['email'=>$email]);
    }

    public function unsubscribe(){
        $data = Input::all();
        if(Request::ajax())
        {
            $email = $data['email'];
            $time = time();
            $date = date('Y-m-d H:i:s',$time);
            $subscribers = Subscribers::where('email',$email)->get();
            foreach ($subscribers as $subscriber){
                $id = $subscriber->id;
            }
            $unsubscribe = Subscribers::find($id);
            $unsubscribe->active = 0;
            $unsubscribe->unsubscribed = $date;
            $unsubscribe->save();
            return 'saved';

        }
        return '';
    }
}