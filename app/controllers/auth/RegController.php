<?php namespace App\Controllers\Auth;

use App\Services\Validators\EmailValidator;
use App\Services\Validators\ActivationValidator;
use App\Models\Subscribers;
use GeneaLabs\Bones\Flash\Flash;

use Auth, BaseController, Form, Input, Redirect, Sentry, View, Mail, Request, Response;

class RegController extends BaseController {

    public function __construct()
    {

    }

    // Register functions
    public function getRegistration()
    {
        return View::make('auth.register');
    }

    public function postRegistration()
    {

        $validator = new EmailValidator;


        if($validator->passes())
        {
            try
            {
                $register = Sentry::createUser(array(
                    'email' => Input::get('email'),
                    'password' => Input::get('temppass'),
                    'activated' => false
                ));

                $activationcode = $register->getActivationCode();
                $league_id = Input::get('leagueId');
                $league_type = Input::get('leagueType');
                $time = time();
                $date = date('Y-m-d H:i:s',$time);
                $subscriber = Subscribers::where('email',Input::get('email'))->get();
                if(count($subscriber) == 0){
                    $subscribe = new Subscribers();
                    $subscribe->email = Input::get('email');
                    $subscribe->active = 1;
                    $subscribe->subscribed = $date;
                    $subscribe->save();
                }
                else{
                    foreach ($subscriber as $element){
                        $id = $element->id;
                        $active = $element->active;
                    }
                    if(isset($active) && $active == 0){
                        $actived = Subscribers::find($id);
                        $actived->active = 1;
                        $actived->unsubscribed = '0000-00-00 00:00:00';
                        $actived->save();
                    }
                }

                Mail::send('users.mails.activation', array('activationcode'=>$activationcode, 'league_id'=>$league_id, 'league_type'=>$league_type), function($message){
                    $message->to(Input::get('email'))->subject('Welcome to the Drafter Fantasy Football League!');
                });
                Flash::success("To activate your account, please check your email. If you did not get a mail, check the spam folder");
                return Redirect::route('auth.login');
            }
            catch (\Cartalyst\Sentry\Users\UserExistsException $e)
            {
                $error = 'The user already exists.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('auth.register')->withErrors(array('register' => $error));

        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }

    public function getActivation($activationcode)
    {
        return \View::make('auth.activation', [ 'activationcode' => $activationcode]);

    }

    public function postActivation($activationcode)
    {

        $name = Input::get('name');
        $pass = Input::get('password');
        $league_id = Input::get('leagueId');
        $league_type = Input::get('leagueType');

        $validator = new ActivationValidator;

        if($validator->passes())
        {
            try
            {
                $user = Sentry::findUserByActivationCode($activationcode);

                if ($user->attemptActivation($activationcode))
                {
                    $user->nickname = $name;
                    $user->password = $pass;
                    $user->activated = true;
                    $user->save();
                    try
                    {
                        if($league_id){
                            Sentry::login($user, false);
                            return Redirect::route('draft', array('id'=>$league_id,'type'=>$league_type));
                        }
                        else{
                            Sentry::login($user, false);
                            return Redirect::route('user.dashboard');
                        }
                    }
                    catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
                    {
                        $error = 'User does not exist.';
                    }
                    catch(\Exception $e)
                    {
                        $error = $e->getMessage();
                    }
                    return Redirect::route('auth.login')->withErrors(array('login' => $error));
                }
                else
                {
                    return Redirect::route('auth.activation')->withErrors(array('activate' => 'Activation failed, please try again.'));
                }
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User does not exist in the database or the account has already been activated.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('auth.activation')->withErrors(array('activate' => $error));
        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }
}