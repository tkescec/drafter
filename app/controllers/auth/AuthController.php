<?php namespace App\Controllers\Auth;

use App\Services\Validators\LoginValidator;
use App\Services\Validators\ForgotValidator;
use App\Services\Validators\ResetValidator;
use GeneaLabs\Bones\Flash\Flash;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Mail;

class AuthController extends BaseController {

    public function __construct()
    {

    }

    // Check if user login, then redirect to dashboard, if not, then render login page.
    public function getLogin()
    {
        if ( ! Sentry::check())
        {
            return View::make('auth.login');
        }
        else
        {
            return Redirect::route('user.dashboard');
        }
    }

    // Check input data. If input data OK, login user and redirect to dashboard, if not, then return error message.
    public function postLogin()
    {
        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        $remember = Input::get('remember');
        $league_id = Input::get('leagueId');
        $league_type = Input::get('leagueType');

        $validator = new LoginValidator;

        if($validator->passes()){
            try
            {
                if(!empty($remember))
                {
                    $user = Sentry::authenticate($credentials, true);
                }
                else
                {
                    $user = Sentry::authenticate($credentials, false);
                }

                if ($user)
                {
                    if($league_id){
                        //return \Redirect::to('draft')->with('id' => $league_id);
                        return Redirect::route('draft', array($league_id,$league_type));
                    }
                    else{
                        return Redirect::route('user.dashboard');
                    }
                }
            }
            catch (\Cartalyst\Sentry\Users\WrongPasswordException $e)
            {
                $error = 'Incorrect password.';
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User does not exist.' ;
            }
            catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
            {
                $error = 'User account is not activated.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('auth.login')->withErrors(array('login' => $error));
        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }

    public function getLogout()
    {
        Sentry::logout();

        return Redirect::route('auth.login');
    }

    public function getForgot()
    {
        return View::make('auth.forgot_password');
    }

    public function postForgot()
    {
        $email = Input::get('email');
        $validator = new ForgotValidator;

        if($validator->passes())
        {
            try
            {
                $user = Sentry::findUserByLogin($email);
                $resetcode = $user->getResetPasswordCode();

                Mail::send('users.mails.password', array('resetcode'=>$resetcode), function($message){
                    $message->to(Input::get('email'))->subject('Request for password reset | Drafter Fantasy Football League!');
                });

                Flash::success("To reset your password, please check your email.");
                return Redirect::route('auth.login');
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User does not exist.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('auth.forgot_password')->withErrors(array('activate' => $error));
        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }

    public function getReset($resetcode)
    {
        return \View::make('auth.reset_password', [ 'resetcode' => $resetcode]);
    }

    public function postReset($resetcode)
    {
        $pass = Input::get('password');

        $validator = new ResetValidator;

        if($validator->passes())
        {
            try
            {
                $user = Sentry::findUserByResetPasswordCode($resetcode);

                if ($user->checkResetPasswordCode($resetcode))
                {
                    if ($user->attemptResetPassword($resetcode, $pass))
                    {
                        Flash::success("You are successfully set a new password.");
                        return Redirect::route('auth.login');
                    }
                    else
                    {
                        return Redirect::route('auth.reset_password')->withErrors(array('new_password' => 'Something went wrong. Please try again.'));
                    }
                }
                else
                {
                    return Redirect::route('auth.reset_password')->withErrors(array('new_password' => 'Invalid password reset code.'));
                }
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User for whom you are trying to change a password does not exist.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('auth.reset_password')->withErrors(array('new_password' => $error));
        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }

}