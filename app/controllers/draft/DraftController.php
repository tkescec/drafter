<?php namespace App\Controllers\Draft;

use App\Models\Leagues;
use App\Models\Players;
use App\Models\PlayersLp;
use App\Models\Games;
use App\Models\Matches;
use App\Models\PlayerPoints;
use App\Models\PlayerStats;
use GeneaLabs\Bones\Flash\Flash;
use BaseController, View, Ajax, Input, Request,Sentry,Mail,Redirect;

class DraftController extends \BaseController {
    public function __construct()
    {
    }
    public function index($id,$type)
    {
        $user = Sentry::getUser();
        if($type == 1){
            $players = PlayersLp::where('inactive',NULL)->orderBy('value', 'DESC')->get();
        }
        else{
            $players = Players::where('inactive',NULL)->orderBy('value', 'DESC')->get();
        }
        $matches = Matches::where('league_id', $id)->orderBy('date','ASC')->orderBy('time','ASC')->get();
        $games = Games::where('league_id', $id)->get();
        return \View::make('draft.index', ['players' => $players, 'matches' => $matches, 'user' => $user, 'games'=>$games])->with('league', Leagues::find($id));
    }
    public function playerFilter()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            $club = $data['club'];
            $position = $data['position'];
            $min = $data['min'];
            $max = $data['max'];
            $type = $data['type'];
            //$league_title = $data['league_title'];
            if($type == 1){
                if($club == 'all' && $position == 'all')
                {
                    $players = PlayersLp::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                if($club == 'all' && $position != 'all')
                {
                    $players = PlayersLp::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('position',$position)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                if($club != 'all' && $position == 'all')
                {
                    $players = PlayersLp::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('club',$club)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                else
                {
                    $players = PlayersLp::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('club',$club)->where('position',$position)->orderBy('value', 'DESC')->get();
                    return $players;
                }
            }
            else{
                if($club == 'all' && $position == 'all')
                {
                    $players = Players::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                if($club == 'all' && $position != 'all')
                {
                    $players = Players::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('position',$position)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                if($club != 'all' && $position == 'all')
                {
                    $players = Players::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('club',$club)->orderBy('value', 'DESC')->get();
                    return $players;
                }
                else
                {
                    $players = Players::where('inactive',NULL)->where('value','>=',$min)->where('value','<=',$max)->where('club',$club)->where('position',$position)->orderBy('value', 'DESC')->get();
                    return $players;
                }
            }
        }
        return '';
    }
    public function playerSearch()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            $name = $data['name'];
            $type = $data['type'];
            if($type == 1){
                $players = PlayersLp::where('inactive',NULL)->where('name',$name)->get();
                return $players;
            }
            else{
                $players = Players::where('inactive',NULL)->where('name',$name)->get();
                return $players;
            }
        }
        return '';
    }
    public function resetFilter()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            $type = $data['type'];
            if($type == 1){
                $players = PlayersLp::where('inactive',NULL)->orderBy('value', 'DESC')->get();
                return $players;
            }
            else{
                $players = Players::where('inactive',NULL)->orderBy('value', 'DESC')->get();
                return $players;
            }
        }
        return '';
    }
    public function checkLogin()
    {
        $data = Input::all();
        $pif = $data['players'];
        $pif = json_decode($pif);
        if(Request::ajax())
        {
            if ( ! Sentry::check())
            {
                return 'no';
            }
            else{
                $league_id = $data['league_id'];
                $num_players = Leagues::where('id',$league_id)->lists('num_players');
                $active_players = Leagues::where('id',$league_id)->lists('active_players');
                $num_players_num = implode(",", $num_players);
                $active_players_num = implode(",", $active_players);
                if($active_players_num >= $num_players_num)
                {
                    return 'full';
                }
                else{
                    $leagues = Leagues::find($league_id);
                    $active_players_new = $active_players_num + 1;
                    $leagues->active_players = $active_players_new;
                    $leagues->save();
                    $games = new Games();
                    $games->user_id = $data['user_id'];
                    $games->league_id = $data['league_id'];
                    $games->pif = $data['players'];
                    $games->formation = $data['formation'];
                    $games->fund_amount = $data['fund'];
                    $games->save();
                    foreach($pif as $value)
                    {
                        $added_players = PlayerPoints::where('player_id',$value->id)->where('league_id',$league_id)->get();
                        if($added_players->isEmpty())
                        {
                            $add_player = new PlayerPoints();
                            $add_player->player_id = $value->id;
                            $add_player->league_id = $data['league_id'];
                            $add_player->save();
                        }
                        $added_players_stats = PlayerStats::where('player_id',$value->id)->where('league_id',$league_id)->get();
                        if($added_players_stats->isEmpty())
                        {
                            $add_player_stats = new PlayerStats();
                            $add_player_stats->player_id = $value->id;
                            $add_player_stats->league_id = $data['league_id'];
                            $add_player_stats->save();
                        }
                    }
                    Flash::success("You have successfully created a team.");
                    return 'yes';
                }
            }
        }
        return '';
    }
    public function sendInvitation()
    {
        $data = Input::all();
        if(Request::ajax())
        {
            Mail::send('users.mails.invite', array('league_id'=>$data['league_id'],'nickname'=>$data['user_name'], 'friend_name'=>$data['friend_name'], 'league_name'=>$data['league_name']), function($message){
                $message->to(Input::get('email'))->subject('Invitation to play game | Drafter Fantasy Football League!');
            });
            return 'sent';
        }
        return '';
    }
}