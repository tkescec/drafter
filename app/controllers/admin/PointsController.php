<?php namespace App\Controllers\Admin;

use App\Models\Players;
use App\Models\PlayersLp;
use App\Models\Games;
use App\Models\Leagues;
use App\Models\Top;
use App\Models\PlayerPoints;
use App\Models\PlayerStats;

use Auth, BaseController, View, Input, Redirect;

class PointsController extends \BaseController {

    public function __construct()
    {

    }
    public function index($id)
    {
        $player_data = array();
        $leagues = Leagues::where('id',$id)->get();
        foreach ($leagues as $league){
            $type = $league->type;
        }
        $players_stats = PlayerStats::where('league_id',$id)->get();
        foreach ($players_stats as $player){
            $player_id = $player->player_id;
            if($type == 1){
                $players = PlayersLp::where('id',$player_id)->get();
            }
            else{
                $players = Players::where('id',$player_id)->get();
            }
            foreach ($players as $value){
                $name = array('name'=>$value->name,'id'=> $value->id,'position'=>$value->position,'club'=>$value->club,'added'=>$player->added,'points'=>$player->all_points,'minutes'=>$player->minutes,'yellow_card'=>$player->yellow_card,'red_card'=>$player->red_card,'own_goal'=>$player->own_goal,'asist'=>$player->asist,'accurate_cross'=>$player->accurate_cross,'missed_penalty'=>$player->missed_penalty,'winning_team'=>$player->winning_team,'goal'=>$player->goal,'pass'=>$player->pass,'shot_on_target'=>$player->shot_on_target,'touches'=>$player->touches,'block'=>$player->block,'interception'=>$player->interception,'rec_goal'=>$player->rec_goal,'first_rec_goal'=>$player->first_rec_goal,'other_rec_goal'=>$player->other_rec_goal,'cleareance'=>$player->cleareance,'dribling'=>$player->dribling,'offside'=>$player->offside,'clean_sheet'=>$player->clean_sheet,'save'=>$player->save,'claim'=>$player->claim,'penalty_saved'=>$player->penalty_saved);
                array_push($player_data,$name);
            }
        }
        function array_sort_by_column(&$arr, $col, $dir = SORT_ASC) {
            $sort_col = array();
            foreach ($arr as $key=> $row) {
                $sort_col[$key] = $row[$col];
            }

            array_multisort($sort_col, $dir, $arr);
        }
        array_sort_by_column($player_data, 'club');
        return \View::make('admin.dashboard.points',['data'=>$player_data, 'league_id'=>$id]);
    }

    public function store($id)
    {
        $minutes=0;$yellow_card=0;$red_card=0;$own_goal=0;$assist=0;$accurate_cross=0;$missed_penalty=0;$winning_team=0;$goal_scored=0;$pass=0;$shot=0;$touches=0;$block=0;$interception=0;$goal_received=0;$first_goal=0;$other_goals=0;$cleareance=0;$dribling=0;$offside=0;$clean_sheet=0;$save=0;$claim=0;$penalty_saved=0;
        $minutes_points=0;$yellow_card_points=0;$red_card_points=0;$own_goal_points=0;$assist_points=0;$accurate_cross_points=0;$missed_penalty_points=0;$winning_team_points=0;$goal_scored_points=0;$pass_points=0;$shot_points=0;$touches_points=0;$block_points=0;$interception_points=0;$goal_received_points=0;$first_goal_points=0;$other_goals_points=0;$cleareance_points=0;$dribling_points=0;$offside_points=0;$clean_sheet_points=0;$save_points=0;$claim_points=0;$penalty_saved_points=0;

        $points_table = Top::find(1);

        //MINUTES
        if (Input::get('minutes')  != null){
            $minutes = (int)Input::get('minutes');
            $minutes_points = $minutes * (float)$points_table->all_M;
            $overall = $minutes_points;
        }
        //YELLOW CARD
        if (Input::get('yellow-card')  != null){
            $yellow_card = (int)Input::get('yellow-card');
            $yellow_card_points = $yellow_card * (float)$points_table->all_YC;
            $overall += $yellow_card_points;
        }
        //RED CARD
        if (Input::get('red-card')  != null){
            $red_card = (int)Input::get('red-card');
            $red_card_points = $red_card * (int)$points_table->all_RC;
            $overall += $red_card_points;
        }
        //OWN GOAL
        if (Input::get('own-goal')  != null){
            $own_goal = (int)Input::get('own-goal');
            $own_goal_points = $own_goal * (int)$points_table->all_OG;
            $overall += $own_goal_points;
        }
        //ASSIST
        if (Input::get('assist')  != null){
            $assist = (int)Input::get('assist');
            $assist_points = $assist * (int)$points_table->all_AS;
            $overall += $assist_points;
        }
        //ACCURATE CROSS
        if (Input::get('accurate-cross')  != null){
            $accurate_cross = (int)Input::get('accurate-cross');
            $accurate_cross_points = $accurate_cross * (float)$points_table->all_AC;
            $overall += $accurate_cross_points;
        }
        //MISSED PENALTY
        if (Input::get('missed-penalty')  != null){
            $missed_penalty = (int)Input::get('missed-penalty');
            $missed_penalty_points = $missed_penalty * (int)$points_table->all_MP;
            $overall += $missed_penalty_points;
        }
        //WINNING TEAM
        if (Input::get('winning-team')  != null){
            $winning_team = (int)Input::get('winning-team');
            $winning_team_points = $winning_team * (float)$points_table->all_WT;
            $overall += $winning_team_points;
        }
        //GOAL SCORED
        if (Input::get('goal-scored')  != null){
            $goal_scored = (int)Input::get('goal-scored');
            $goal_scored_points = $goal_scored * (int)$points_table->at_Goal;
            $overall += $goal_scored_points;
        }
        //PASS
        if (Input::get('pass-df')  != null){
            $pass = (int)Input::get('pass-df');
            $pass_points = $pass * (float)$points_table->df_Pass;
            $overall += $pass_points;
        }
        if (Input::get('pass-mf') != null){
            $pass = (int)Input::get('pass-mf');
            $pass_points = $pass * (float)$points_table->mf_Pass;
            $overall += $pass_points;
        }
        if (Input::get('pass-at') != null){
            $pass = (int)Input::get('pass-at');
            $pass_points = $pass * (float)$points_table->at_Pass;
            $overall += $pass_points;
        }
        //SHOT ON TARGET
        if (Input::get('shot-df')  != null){
            $shot = (int)Input::get('shot-df');
            $shot_points = $shot * (int)$points_table->df_SOT;
            $overall += $shot_points;
        }
        if (Input::get('shot-mf') != null){
            $shot = (int)Input::get('shot-mf');
            $shot_points = $shot * (float)$points_table->mf_SOT;
            $overall += $shot_points;
        }
        if (Input::get('shot-at') != null){
            $shot = (int)Input::get('shot-at');
            $shot_points = $shot * (float)$points_table->at_SOT;
            $overall += $shot_points;
        }
        //TOUCHES
        if (Input::get('touches-mf') != null){
            $touches = (int)Input::get('touches-mf');
            $touches_points = $touches * (float)$points_table->mf_Touches;
            $overall += $touches_points;
        }
        if (Input::get('touches-at') != null){
            $touches = (int)Input::get('touches-at');
            $touches_points = $touches * (float)$points_table->at_Touches;
            $overall += $touches_points;
        }
        //OFFSIDE
        if (Input::get('offside-at') != null){
            $offside = (int)Input::get('offside-at');
            $offside_points = $offside * (float)$points_table->at_Offside;
            $overall += $offside_points;
        }
        //INTERCEPTION
        if (Input::get('interception-df')  != null){
            $interception = (int)Input::get('interception-df');
            $interception_points = $interception * (int)$points_table->df_Interception;
            $overall += $interception_points;
        }
        if (Input::get('interception-mf') != null){
            $interception = (int)Input::get('interception-mf');
            $interception_points = $interception * (float)$points_table->mf_Interception;
            $overall += $interception_points;
        }
        if (Input::get('interception-at') != null){
            $interception = (int)Input::get('interception-at');
            $interception_points = $interception * (float)$points_table->at_Interception;
            $overall += $interception_points;
        }
        //CLEAREANCE
        if (Input::get('cleareance-df')  != null){
            $cleareance = (int)Input::get('cleareance-df');
            $cleareance_points = $cleareance * (float)$points_table->df_Cleareance;
            $overall += $cleareance_points;
        }
        if (Input::get('cleareance-mf') != null){
            $cleareance = (int)Input::get('cleareance-mf');
            $cleareance_points = $cleareance * (float)$points_table->mf_Cleareance;
            $overall += $cleareance_points;
        }
        if (Input::get('cleareance-at') != null){
            $cleareance = (int)Input::get('cleareance-at');
            $cleareance_points = $cleareance * (float)$points_table->at_Cleareance;
            $overall += $cleareance_points;
        }
        //DRIBBLING
        if (Input::get('dribling-mf') != null){
            $dribling = (int)Input::get('dribling-mf');
            $dribling_points = $dribling * (float)$points_table->mf_Dribling;
            $overall += $dribling_points;
        }
        if (Input::get('dribling-at') != null){
            $dribling = (int)Input::get('dribling-at');
            $dribling_points = $dribling * (float)$points_table->at_Dribling;
            $overall += $dribling_points;
        }
        //CLEAN SHEET
        if (Input::get('clean-sheet-gk')  != null){
            $clean_sheet = (int)Input::get('clean-sheet-gk');
            $clean_sheet_points = $clean_sheet * (int)$points_table->gk_CS;
            $overall += $clean_sheet_points;
        }
        if (Input::get('clean-sheet-df')  != null){
            $clean_sheet = (int)Input::get('clean-sheet-df');
            $clean_sheet_points = $clean_sheet * (int)$points_table->df_CS;
            $overall += $clean_sheet_points;
        }
        //BLOCK
        if (Input::get('block-df')  != null){
            $block = (int)Input::get('block-df');
            $block_points = $block * (float)$points_table->df_Block;
            $overall += $block_points;
        }
        //GOAL RECEIVED
        if (Input::get('goal-received-df')  != null){
            $goal_received = (int)Input::get('goal-received-df');
            $goal_received_points = $goal_received * (int)$points_table->df_GR;
            $overall += $goal_received_points;
        }
        if (Input::get('goal-received-mf') != null){
            $goal_received = (int)Input::get('goal-received-mf');
            $goal_received_points = $goal_received * (float)$points_table->mf_GR;
            $overall += $goal_received_points;
        }
        //SAVE
        if (Input::get('save-gk')  != null){
            $save = (int)Input::get('save-gk');
            $save_points = $save * (float)$points_table->gk_Save;
            $overall += $save_points;
        }
        //PENALTY SAVED
        if (Input::get('penalty-saved-gk')  != null){
            $penalty_saved = (int)Input::get('penalty-saved-gk');
            $penalty_saved_points = $penalty_saved * (int)$points_table->gk_PS;
            $overall += $penalty_saved_points;
        }
        //CLAIM
        if (Input::get('claim-gk')  != null){
            $claim = (int)Input::get('claim-gk');
            $claim_points = $claim * (float)$points_table->gk_Claim;
            $overall += $claim_points;
        }
        //FIRST GOAL
        if (Input::get('first-goal-gk')  != null){
            $first_goal = (int)Input::get('first-goal-gk');
            $first_goal_points = $first_goal * (int)$points_table->gk_FGR;
            $overall += $first_goal_points;
        }
        //OTHER GOALS
        if (Input::get('other-goals-gk')  != null){
            $other_goals = (int)Input::get('other-goals-gk');
            $other_goals_points = $other_goals * (int)$points_table->gk_OGR;
            $overall += $other_goals_points;
        }

        $player_stats = PlayerStats::where('player_id',Input::get('player_id'))->where('league_id',$id)->update(array('minutes'=>$minutes,'yellow_card'=>$yellow_card,'red_card'=>$red_card,'own_goal'=>$own_goal,'asist'=>$assist,'accurate_cross'=>$accurate_cross,'missed_penalty'=>$missed_penalty,'winning_team'=>$winning_team,'goal'=>$goal_scored,'pass'=>$pass,'shot_on_target'=>$shot,'touches'=>$touches,'block'=>$block,'interception'=>$interception,'rec_goal'=>$goal_received,'first_rec_goal'=>$first_goal,'other_rec_goal'=>$other_goals,'cleareance'=>$cleareance,'dribling'=>$dribling,'offside'=>$offside,'clean_sheet'=>$clean_sheet,'save'=>$save,'claim'=>$claim,'penalty_saved'=>$penalty_saved,'all_points'=>$overall,'added'=>1));
        $player_points = PlayerPoints::where('player_id',Input::get('player_id'))->where('league_id',$id)->update(array('minutes'=>$minutes_points,'yellow_card'=>$yellow_card_points,'red_card'=>$red_card_points,'own_goal'=>$own_goal_points,'asist'=>$assist_points,'accurate_cross'=>$accurate_cross_points,'missed_penalty'=>$missed_penalty_points,'winning_team'=>$winning_team_points,'goal'=>$goal_scored_points,'pass'=>$pass_points,'shot_on_target'=>$shot_points,'touches'=>$touches_points,'block'=>$block_points,'interception'=>$interception_points,'rec_goal'=>$goal_received_points,'first_rec_goal'=>$first_goal_points,'other_rec_goal'=>$other_goals_points,'cleareance'=>$cleareance_points,'dribling'=>$dribling_points,'offside'=>$offside_points,'clean_sheet'=>$clean_sheet_points,'save'=>$save_points,'claim'=>$claim_points,'penalty_saved'=>$penalty_saved_points,'all_points'=>$overall,'added'=>1));

        $games = Games::where('league_id',$id)->get();
        foreach($games as $game){
            $user_id = $game->user_id;
            $pif = $game->pif;
            $pif = json_decode($pif);
            $score = $game->score;
            $score += $overall;
            $data = array($user_id => $pif);
            foreach ($data as $index => $user){
                switch(Input::get('player_id')){
                    case $user[0]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[1]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[2]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[3]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[4]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[5]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[6]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[7]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[8]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[9]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                    case $user[10]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$id)->update(array('score'=>$score));
                        break;
                }
            }
        }
        return Redirect::route('admin.dashboard.points',$id);

    }

    public function edit($league_id,$id)
    {
        $reset_player_stats = PlayerStats::where('player_id',$id)->where('league_id',$league_id)->update(array('added'=>null));
        $reset_player_points = PlayerPoints::where('player_id',$id)->where('league_id',$league_id)->update(array('added'=>null));
        $player_points = PlayerPoints::where('player_id',$id)->where('league_id',$league_id)->get();
        foreach ($player_points as $points){
            $all_points = $points->all_points;
        }
        $games = Games::where('league_id',$league_id)->get();
        foreach ($games as $game){
            $user_id = $game->user_id;
            $pif = $game->pif;
            $pif = json_decode($pif);
            $score = $game->score;
            $score -= $all_points;
            $data = array($user_id => $pif);
            foreach ($data as $index => $user){
                switch($id){
                    case $user[0]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[1]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[2]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[3]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[4]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[5]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[6]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[7]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[8]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[9]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                    case $user[10]->id:
                        $games = Games::where('user_id',$index)->where('league_id',$league_id)->update(array('score'=>$score));
                        break;
                }
            }
        }
        return Redirect::route('admin.dashboard.points',$league_id);
    }
}