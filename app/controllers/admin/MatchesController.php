<?php namespace App\Controllers\Admin;

use App\Models\Matches;
use App\Models\Clubs;
use App\Services\Validators\MatchesValidator;
use GeneaLabs\Bones\Flash\Flash;

use Auth, BaseController, Form, Input, Redirect, View, URL;

class MatchesController extends \BaseController {

    public function __construct()
    {

    }

    public function index($id){
        $matches = Matches::where('league_id', $id)->orderBy('date','ASC')->orderBy('time','ASC')->get();
        return \View::make('admin.dashboard.results', ['matches' => $matches]);
    }

    public function update($id){
        $result = Input::get('result');
        $match = Matches::find($id);
        if(empty($result)){
            $match->results = '0:0';
            $match->save();
            return Redirect::route('admin.dashboard.results',$match->league_id);
        }
        else{
            $match->results = $result;
            $match->save();
            return Redirect::route('admin.dashboard.results',$match->league_id);
        }
    }

    public function create($id){
        $matches = Matches::where('league_id', $id)->orderBy('date','ASC')->orderBy('time','ASC')->get();
        $clubs_array = array();
        $clubs = Clubs::all();
        foreach($clubs as $club){
            $clubs_array = array_merge($clubs_array, array($club->club_name => $club->club_name));
        }
        return \View::make('admin.matches.create', ['clubs' => $clubs_array,'league_id' => $id, 'matches' => $matches]);
    }

    public function store(){
        $matches = Matches::where('league_id', Input::get('league_id'))->orderBy('date','ASC')->orderBy('time','ASC')->get();
        $validator = new MatchesValidator;
        if ( $validator->passes() )
        {
            $date = Input::get('match-date');
            $explodedate = explode('/', $date);
            $newdate = $explodedate[2].'-'.$explodedate[1].'-'.$explodedate[0];
            $match = new Matches();
            $match->club1 = Input::get('home');
            $match->club2 = Input::get('away');
            $match->date = $newdate;
            $match->time = Input::get('match-time');
            $match->results = '-';
            $match->league_id = Input::get('league_id');

            $match->save();

            Flash::success("You have successfully add a new match.");
            return Redirect::route('admin.matches.create',['league_id' => Input::get('league_id'), 'matches' => $matches]);
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function edit($id){
        $match = Matches::find($id);
        $league_id = $match->league_id;
        $clubs_array = array();
        $clubs = Clubs::all();
        foreach($clubs as $club){
            $clubs_array = array_merge($clubs_array, array($club->club_name => $club->club_name));
        }
        return \View::make('admin.matches.edit', ['match' => $match,'league_id' =>$league_id,'clubs' => $clubs_array]);
    }

    public function updateMatch($id){

        $validator = new MatchesValidator;
        if ( $validator->passes() )
        {
            $date = Input::get('match-date');
            $explodedate = explode('/', $date);
            $newdate = $explodedate[2].'-'.$explodedate[1].'-'.$explodedate[0];
            $match = Matches::find($id);
            $match->club1 = Input::get('home');
            $match->club2 = Input::get('away');
            $match->date = $newdate;
            $match->time = Input::get('match-time');
            $match->results = '-';
            $match->league_id = Input::get('league_id');

            $match->save();

            Flash::success("You have successfully edit a match.");
            return Redirect::route('admin.matches.create',['league_id' => Input::get('league_id')]);
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function destroy($id){
        $match = Matches::find($id);
        $match->delete();
        Flash::success("You have successfully delete the match.");
        return Redirect::route('admin.matches.create',['league_id' => $match->league_id]);
    }
}