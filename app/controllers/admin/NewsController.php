<?php namespace App\Controllers\Admin;

use App\Models\NewsFeed;
use App\Services\Validators\NewsValidator;
use GeneaLabs\Bones\Flash\Flash;

use Auth, BaseController, Form, Input, Redirect, View, URL, File;

class NewsController extends \BaseController {

    public function __construct()
    {

    }

    public function create(){
        return \View::make('admin.news.create');
    }

    public function store(){
        $validator = new NewsValidator;
        if ( $validator->passes())
        {
            $news = new NewsFeed();
            $news->title = Input::get('news-title');
            $news->content = Input::get('news-content');

            $news->save();

            Flash::success("You have successfully add a news.");
            return Redirect::route('admin.dashboard');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function edit($id){
        return \View::make('admin.news.edit')->with('news', NewsFeed::find($id));
    }

    public function update($id){
        $validator = new NewsValidator;
        if ( $validator->passes())
        {
            $news = NewsFeed::find($id);
            $news->title = Input::get('news-title');
            $news->content = Input::get('news-content');

            $news->save();

            Flash::success("You have successfully update ".Input::get('news-title'));
            return Redirect::route('admin.dashboard');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function destroy($id)
    {
        $news = NewsFeed::find($id);
        $news->delete();
        Flash::success("You have successfully deleted a news.");
        return Redirect::route('admin.dashboard');
    }

}