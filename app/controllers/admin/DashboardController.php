<?php namespace App\Controllers\Admin;

use App\Models\Leagues;
use App\Models\NewsFeed;

use Auth, BaseController, View;

class DashboardController extends \BaseController {

    public function __construct()
    {

    }
    public function index()
    {
        $leagues = Leagues::orderBy('date','DESC')->get();
        $news = NewsFeed::orderBy('created_at','DESC')->get();
        return \View::make('admin.dashboard.index', ['leagues' => $leagues,'news' => $news]);
    }
}