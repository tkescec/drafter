<?php namespace App\Controllers\Admin;

use App\Services\Validators\LoginValidator;
use GeneaLabs\Bones\Flash\Flash;
use Auth, BaseController, Form, Input, Redirect, Sentry, View;

class AuthController extends BaseController {

    public function __construct()
    {

    }

    public function index()
    {
        if ( ! Sentry::check())
        {
            return View::make('admin.index');
        }
        else
        {
            try
            {
                $user = Sentry::getUser();
                $admin = Sentry::findGroupByName('Administrator');

                if ($user->inGroup($admin))
                {
                    return Redirect::route('admin.dashboard');
                }
                else
                {
                    Flash::danger("Access denied.");
                    return Redirect::route('/');
                }
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User does not exist in the database.' ;
            }
            catch (\Cartalyst\Sentry\Users\GroupNotFoundException $e)
            {
                $error = 'Group was not found.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('admin')->withErrors(array('admin' => $error));
        }
    }

    public function postLogin()
    {
        $credentials = array(
            'email' => Input::get('email'),
            'password' => Input::get('password')
        );

        $remember = Input::get('remember');

        $validator = new LoginValidator;

        if($validator->passes()){
            try
            {
                $admin = Sentry::findGroupByName('Administrator');
                $checkuser = Sentry::findUserByLogin(Input::get('email'));

                if ($checkuser->inGroup($admin))
                {
                    try
                    {
                        if(!empty($remember))
                        {
                            $user = Sentry::authenticate($credentials, true);
                        }
                        else
                        {
                            $user = Sentry::authenticate($credentials, false);
                        }
                        if($user)
                        {
                            return Redirect::route('admin.dashboard');
                        }
                    }
                    catch (\Cartalyst\Sentry\Users\WrongPasswordException $e)
                    {
                        $error = 'Incorrect password.';
                    }
                    catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
                    {
                        $error = 'User does not exist in the database.' ;
                    }
                    catch (\Cartalyst\Sentry\Users\UserNotActivatedException $e)
                    {
                        $error = 'User account is not activated.';
                    }
                    catch(\Exception $e)
                    {
                        $error = $e->getMessage();
                    }
                    return Redirect::route('admin')->withErrors(array('admin' => $error));
                }
                else
                {
                    Flash::danger("Access denied.");
                    return Redirect::route('admin');
                }
            }
            catch (\Cartalyst\Sentry\Users\UserNotFoundException $e)
            {
                $error = 'User does not exist in the database.' ;
            }
            catch (\Cartalyst\Sentry\Users\GroupNotFoundException $e)
            {
                $error = 'Group was not found.';
            }
            catch(\Exception $e)
            {
                $error = $e->getMessage();
            }
            return Redirect::route('admin')->withErrors(array('admin' => $error));
        }
        return Redirect::back()->withInput()->withErrors($validator->errors);
    }

    public function getLogout()
    {
        Sentry::logout();

        return Redirect::route('admin');
    }

}