<?php namespace App\Controllers\Admin;

use App\Models\Leagues;
use App\Services\Validators\LeaguesValidator;
use GeneaLabs\Bones\Flash\Flash;

use Auth, BaseController, Form, Input, Redirect, View, URL, File;

class LeaguesController extends \BaseController {

    public function __construct()
    {

    }
    public function index()
    {
        $leagues = Leagues::orderBy('date','DESC')->get();
        return \View::make('admin.leagues.index', ['leagues' => $leagues]);
    }

    public function create()
    {
        $files = File::allFiles(public_path().'/uploads');
        return \View::make('admin.leagues.create',  ['files' => $files]);
    }

    public function store()
    {

        $validator = new LeaguesValidator;
        if ( $validator->passes() )
        {
            $date = Input::get('application-date');
            $explodedate = explode('/', $date);
            $newdate = $explodedate[2].'-'.$explodedate[1].'-'.$explodedate[0];
            if(Input::file('league-logo-file'))
            {
                $filename= Input::file('league-logo-file')->getClientOriginalName();
                Input::file('league-logo-file')->move(public_path() .'/uploads',$filename);
                $filepath = URL::to('uploads/' . $filename);
            }
            else
            {
                $filepath = Input::get('league-logo');
            }
            $league = new Leagues();
            $league->title = Input::get('league-title');
            $league->logo = $filepath;
            $league->salary_cup = Input::get('salary-cap');
            $league->season = Input::get('season');
            $league->round = Input::get('round');
            $league->type = Input::get('type');
            $league->entry_fee = Input::get('entry-fee');
            $league->num_players = Input::get('max-players');
            $league->date = $newdate;
            $league->time = Input::get('application-time');

            $league->save();

            Flash::success("You have successfully created a new league.");
            return Redirect::route('admin.dashboard');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $files = File::allFiles(public_path().'/uploads');
        return \View::make('admin.leagues.edit',  ['files' => $files])->with('league', Leagues::find($id));
    }

    public function update($id)
    {

        $validator = new LeaguesValidator;

        if ( $validator->passes() )
        {
            $date = Input::get('application-date');
            $explodedate = explode('/', $date);
            $newdate = $explodedate[2].'-'.$explodedate[1].'-'.$explodedate[0];
            if(Input::file('league-logo-file'))
            {
                $filename= Input::file('league-logo-file')->getClientOriginalName();
                Input::file('league-logo-file')->move(public_path() .'/uploads',$filename);
                $filepath = URL::to('uploads/' . $filename);
            }
            else
            {
                $filepath = Input::get('league-logo');
            }
            $league = Leagues::find($id);
            $league->title = Input::get('league-title');
            $league->logo = $filepath;
            $league->salary_cup = Input::get('salary-cap');
            $league->season = Input::get('season');
            $league->round = Input::get('round');
            $league->type = Input::get('type');
            $league->entry_fee = Input::get('entry-fee');
            $league->num_players = Input::get('max-players');
            $league->date = $newdate;
            $league->time = Input::get('application-time');

            $league->save();

            Flash::success("You have successfully update ".Input::get('league-title'));
            return Redirect::route('admin.dashboard');
        }
        else
        {
            return Redirect::back()->withInput()->withErrors($validator->errors);
        }
    }

    public function destroy($id)
    {
        $league = Leagues::find($id);
        $league->delete();
        Flash::success("You have successfully deleted the league.");
        return Redirect::route('admin.dashboard');
    }

}