<?php namespace App\Controllers\Leagues;

use App\Models\Leagues;
use App\Models\Matches;
use App\Models\Prizes;
use App\Models\Games;

use BaseController, View, Ajax, Input, Request;

class LeaguesController extends \BaseController {

    public function __construct()
    {

    }
    public function index()
    {
        $leagues = Leagues::orderBy('date','DESC')->get();
        return \View::make('leagues.index', ['leagues' => $leagues]);
    }

    public function matches(){
        $data = Input::all();
        if(Request::ajax())
        {
            $matches = Matches::where('league_id', $data['league_id'])->orderBy('date','ASC')->orderBy('time','ASC')->get();
            return $matches;
        }
        return '';
    }

    public function prizes(){
        $data = Input::all();
        if(Request::ajax())
        {
            $prizes = Prizes::where('league_id', $data['league_id'])->orderBy('position','ASC')->get();
            return $prizes;
        }
        return '';
    }

    public function players(){
        $data = Input::all();
        if(Request::ajax())
        {
            $games = Games::where('league_id', $data['league_id'])->orderBy('id','ASC')->with('competitors')->get();
            return $games;
        }
        return '';
    }

}