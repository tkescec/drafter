<?php namespace App\Controllers;

use BaseController, View;

class HomeController extends BaseController {

    public function __construct()
    {

    }

	public function index()
	{
		return View::make('index');
	}

    public function rules()
    {
        return View::make('rules.index');
    }


}
