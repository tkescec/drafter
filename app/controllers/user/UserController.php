<?php namespace App\Controllers\User;

use App\Models\NewsFeed;
use App\Models\Players;
use App\Models\PlayersLp;
use App\Models\Leagues;
use App\Models\Games;
use App\Models\Matches;
use App\Models\PlayerPoints;
use GeneaLabs\Bones\Flash\Flash;
use Auth, BaseController, Form, Input, Redirect, Sentry, View, Response, Request, Ajax, Mail;

class UserController extends \BaseController {
    public function __construct()
    {
    }
    public function dashboard()
    {
        $user = Sentry::getUser();
        $games = Games::where('user_id',$user->id)->orderBy('id', 'DESC')->with(array('leagues'=>function($query){$query->orderBy('date', 'DESC')->orderBy('time', 'DESC');}))->get();
        $news = NewsFeed::orderBy('created_at','DESC')->paginate(5);
        return \View::make('user.dashboard',['games' => $games,'user'=>$user, 'news' => $news]);
    }
    public function gameEdit($id){
        $user = Sentry::getUser();
        $games = Games::where('id',$id)->with('leagues')->get();
        foreach ($games as $game){
            $league_id = $game->league_id;
        }
        $matches = Matches::where('league_id', $league_id)->orderBy('date','ASC')->orderBy('time','ASC')->get();
        $leagues = Leagues::where('id',$league_id)->get();
        foreach ($leagues as $league){
            $type = $league->type;
        }
        if($type == 1){
            $players = PlayersLp::where('inactive',NULL)->orderBy('value', 'DESC')->get();
        }
        else{
            $players = Players::where('inactive',NULL)->orderBy('value', 'DESC')->get();
        }
        return \View::make('user.game.index',['games' => $games,'players' => $players,'user' => $user, 'matches' => $matches]);
    }
    public function gameResults($id)
    {
        $games = Games::where('league_id',$id)->orderBy('score', 'DESC')->with('competitors')->with('leagues')->get();
        foreach($games as $game){
            $league_id = $game->league_id;
        }
        $leagues = Leagues::where('id',$league_id)->get();
        foreach ($leagues as $league){
            $type = $league->type;
        }
        if($type == 1){
            $players = PlayersLp::all();
        }
        else{
            $players = Players::all();
        }
        foreach ($games as $game){
            $user_id = $game->user_id;
            $league_id = $game->league_id;
            $pif = $game->pif;
            $pif = json_decode($pif);
            $data = array($user_id => $pif);
            foreach ($data as $index => $user){
                $player0 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[0]->id)->get();
                foreach($player0 as $value){
                    $data[$index][0]->points = $value->all_points;
                }
                $player1 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[1]->id)->get();
                foreach($player1 as $value){
                    $data[$index][1]->points = $value->all_points;
                }
                $player2 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[2]->id)->get();
                foreach($player2 as $value){
                    $data[$index][2]->points = $value->all_points;
                }
                $player3 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[3]->id)->get();
                foreach($player3 as $value){
                    $data[$index][3]->points = $value->all_points;
                }
                $player4 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[4]->id)->get();
                foreach($player4 as $value){
                    $data[$index][4]->points = $value->all_points;
                }
                $player5 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[5]->id)->get();
                foreach($player5 as $value){
                    $data[$index][5]->points = $value->all_points;
                }
                $player6 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[6]->id)->get();
                foreach($player6 as $value){
                    $data[$index][6]->points = $value->all_points;
                }
                $player7 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[7]->id)->get();
                foreach($player7 as $value){
                    $data[$index][7]->points = $value->all_points;
                }
                $player8 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[8]->id)->get();
                foreach($player8 as $value){
                    $data[$index][8]->points = $value->all_points;
                }
                $player9 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[9]->id)->get();
                foreach($player9 as $value){
                    $data[$index][9]->points = $value->all_points;
                }
                $player10 = PlayerPoints::where('league_id',$league_id)->where('player_id',$user[10]->id)->get();
                foreach($player10 as $value){
                    $data[$index][10]->points = $value->all_points;
                }
                $user = json_encode($user);
                $add_data = Games::where('league_id',$league_id)->where('user_id',$index)->update(array('pif' => $user));
            }
        }
        return \View::make('user.game.results',['games'=>$games,'players'=>$players]);
    }
    public function gameTeam($id)
    {
        $games = Games::where('id',$id)->get();
        foreach($games as $game){
            $league_id = $game->league_id;
        }
        $matches = Matches::where('league_id', $league_id)->orderBy('date','ASC')->orderBy('time','ASC')->get();
        $leagues = Leagues::where('id',$league_id)->get();
        foreach ($leagues as $league){
            $type = $league->type;
        }
        if($type == 1){
            $players = PlayersLp::all();
        }
        else{
            $players = Players::all();
        }
        return \View::make('user.game.team',['games'=>$games,'players'=>$players,'matches'=>$matches]);
    }
    public function gameUpdate(){
        $data = Input::all();
        $pif = $data['players'];
        $pif = json_decode($pif);
        if(Request::ajax())
        {
            if ( ! Sentry::check())
            {
                return 'no';
            }
            else
            {
                $game = Games::find($data['game_id']);
                $game->pif = $data['players'];
                $game->formation = $data['formation'];
                $game->fund_amount = $data['fund'];
                $game->save();
                foreach($pif as $value){
                    $added_players = PlayerPoints::where('player_id',$value->id)->where('league_id',$data['league_id'])->get();
                    if($added_players->isEmpty()){
                        $add_player = new PlayerPoints();
                        $add_player->player_id = $value->id;
                        $add_player->league_id = $data['league_id'];
                        $add_player->save();
                    }
                }
                Flash::success("You have successfully update your team.");
                return 'yes';
            }
        }
        return '';
    }
    public function gamePosition()
    {
        $data = Input::all();
        $position = $data['position'];
        $nickname = $data['nickname'];
        $league_id = $data['league_id'];
        if(Request::ajax())
        {
            $game = Games::where('user_id',$nickname)->where('league_id',$league_id)->update(array('position' => $position));
            return 'saved';
        }
        return '';
    }
    public function gameStats()
    {
        $data = Input::all();
        $player_id = $data['player_id'];
        $league_id = $data['league_id'];
        if(Request::ajax())
        {
            $player = PlayerPoints::where('player_id',$player_id)->where('league_id',$league_id)->get();
            return $player;
        }
        return '';
    }
}